{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE LambdaCase #-}

module Reduce.ImportNormalizationSpec where

-- elm-compiler
import qualified AST.Source as Src
import qualified Elm.Package as Pkg
import qualified Elm.Compiler.Imports as ElmImports

-- misc
import Test.Hspec
import Control.Lens
import Data.Text (Text)
-- import qualified Data.Text.IO as Text
import Text.RawString.QQ (r)
import Control.Monad
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

import qualified Reduce.ImportNormalization.FullyQualifyIdentifiers as FullyQualifyIdentifiers
import qualified Reduce.ImportNormalization as ImportNormalization
import Reduce.ImportNormalization.Types
import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.AST as AST
import TestUtil


-- Note: Since I changed the way that parsing module exports works, this is not as
-- interesting of a test anymore: At some point I just apply `filter` on my list of
-- exports. So this property is now pretty much given because of `filter`.
-- It used to be different, in that I would do different things, depending on whether
-- the export list is `Src.Open` or `Src.Explicit exposes`, but its not anymore.
openExportSupersetTest :: FilePath -> IO ()
openExportSupersetTest modulePath = do
    moduleWithRestrictedExports <- Elm.parseModuleFromFile Pkg.dummyName modulePath
    let
        isSubsetOf subset superset =
            all (`elem` superset) subset

        moduleWithOpenExports =
            set (Elm.moduleExports . Elm.valueOfLocated) Src.Open moduleWithRestrictedExports

        exportsRestricted =
            ImportNormalization.moduleExports moduleWithRestrictedExports

        exportsOpen =
            ImportNormalization.moduleExports moduleWithOpenExports

    exportsRestricted `shouldSatisfy` (`isSubsetOf` exportsOpen)


testInnerSource :: Text
testInnerSource = [r|
type Union1 b
    = Constr1
    | Constr2 Int Int
    | Constr3 (b -> Int)

type alias Alias1 =
    ()

type alias Alias2 =
    { field1 : Int
    , field2 : Int
    , etc : ()
    }

type Union2 x y z
    = Union2 x y z

function1 : Int -> Int
function1 x = x

function2 =
    let
        local x =
            x
    in
    local

function3 : Union2 x y z -> Union2 x y z
function3 = identity
|]


testSourceWithFirstLineShouldExpose :: Text -> [IdentifierDeclaration] -> Expectation
testSourceWithFirstLineShouldExpose firstLine expectingToExpose = do
    moduleExports <-
        case Elm.parseModule Pkg.dummyName (firstLine <> "\n" <> testInnerSource) of
            Right module_ ->
                return (ImportNormalization.moduleExports module_)

            Left reports -> do
                Elm.printReports "Main" reports
                expectationFailure "Failed to parse test file"
                return []

    moduleExports `shouldMatchList` expectingToExpose


spec :: Spec
spec = do
    describe "moduleExports" $ do
        it "passes simple golden tests for a test module" $ do
            testSourceWithFirstLineShouldExpose "module Main exposing (..)"
                [ UnionType "Union1"
                , UnionConstructor "Union1" "Constr1"
                , UnionConstructor "Union1" "Constr2"
                , UnionConstructor "Union1" "Constr3"
                , UnionType "Union2"
                , UnionConstructor "Union2" "Union2"
                , AliasType "Alias1"
                -- , AliasConstructor "Alias1" It should _not_ contain this constructor. Type aliases only generate a constructor when they're an alias for records!
                , AliasType "Alias2"
                , AliasConstructor "Alias2"
                , FunctionDeclaration "function1"
                , FunctionDeclaration "function2"
                , FunctionDeclaration "function3"
                ]

            testSourceWithFirstLineShouldExpose "module Main exposing (Union2(..), Union1, Alias1, function2)"
                [ UnionType "Union1"
                , UnionType "Union2"
                , UnionConstructor "Union2" "Union2"
                , AliasType "Alias1"
                -- , AliasConstructor "Alias1" Same as above
                , FunctionDeclaration "function2"
                ]

        forProjectsElmFilesIt
            [ "test-data/elm-spa-example"
            , "test-data/minimal"
            ]
            "gives a superset of results when replacing with open exports"
            (\_ _ -> openExportSupersetTest)

    describe "moduleImportDict" $ do

        importDictionary <- runIO $ do
            summary <- Elm.parseProject "test-data/minimal"
            module_ <- Elm.parseModuleFromFile Pkg.dummyName "test-data/minimal/TestModule.elm"
            ImportNormalization.moduleImportDict summary module_

        mapM_
            (\(identifier, moduleName) ->
                it ("should, for test-data/minimal/TestModule.elm contain the entry " ++ show (identifier, moduleName)) $ do
                    Map.lookup identifier importDictionary `shouldBe` Just moduleName
            )
            [ (Id FunctionNamespace Nothing "dependency", "Dependency")
            , (Id FunctionNamespace Nothing "div", "Html")
            , (Id FunctionNamespace (Just "Html") "div", "Html")
            , (Id FunctionNamespace (Just "String") "fromInt", "String")
            , (Id FunctionNamespace (Just "Browsie") "sandbox", "Browser")
            ]

        mapM_
            (\(identifier, moduleName) ->
                it ("should, for test-data/minimal/TestModule.elm **not** contain the entry " ++ show (identifier, moduleName)) $ do
                    Map.lookup identifier importDictionary `shouldNotBe` Just moduleName
            )
            [ (Id FunctionNamespace (Just "Browser") "sandbox", "Browser")
            ]

    describe "normalizeImports" $ do
        it "should not import any non-basic identifiers from other modules, except for operators" $ do
            let modulePath = "test-data/minimal/TestModule.elm"

            summary <- Elm.parseProject "test-data/minimal"
            module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath
            moduleQualified <- ImportNormalization.normalizeImports summary module_

            let
                isNonQualifiedImport = \case
                    Id _ qualifier _ ->
                        Maybe.isJust qualifier
                    
                    Op _ ->
                        True
                    
                    ListId ->
                        True

                isDefaultImported moduleName =
                    any 
                        ((== moduleName) . view (Elm.importName . Elm.valueOfLocated))
                        ElmImports.defaults

            moduleQualifiedReparsed <-
                Elm.parseModuleOrCrash Pkg.dummyName modulePath
                    (AST.renderModule (AST.translateModule moduleQualified))

            nonDefaultImportedIds <-
                Map.filter (not . isDefaultImported) <$>
                    ImportNormalization.moduleImportDict summary moduleQualifiedReparsed

            forM_ (Map.keys nonDefaultImportedIds) (`shouldSatisfy` isNonQualifiedImport)

        forProjectsElmFilesIt
            [ "test-data/elm-spa-example"
            , "test-data/minimal"
            ]
            "should keep well-typed-ness" $ testFileTransformTypeCheck $ \_ summary testFile content -> do
                module_ <- Elm.parseModuleOrCrash (Elm.summaryPkgName summary) testFile content
                importDict <- ImportNormalization.moduleImportDict summary module_

                module_
                    & FullyQualifyIdentifiers.inModule importDict
                    & AST.translateModule
                    & AST.renderModule
                    & return
