{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}

module TestUtil where

-- misc
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Test.Hspec
import qualified Text.PrettyPrint as Doc
import qualified Data.Algorithm.DiffContext as Diff
import Control.Monad
import qualified Control.Exception as Exception
import qualified Data.Map as Map

-- elm-compiler
import qualified Elm.Project.Summary as Summary
import qualified Reporting.Exit as Exit

import qualified Language.Elm as Elm


-- type Expectation = IO ()
shouldNotBeDifferentTo :: (String, Text) -> (String, Text) -> Expectation
shouldNotBeDifferentTo (nameA, textA) (nameB, textB) =
    let
        -- like a git diff
        diff =
            Diff.getContextDiff 4 (Text.lines textA) (Text.lines textB)

        renderedDiff =
            Doc.renderStyle Doc.style
                (Diff.prettyContextDiff
                    (Doc.text nameA)
                    (Doc.text nameB)
                    (Doc.text . Text.unpack)
                    diff)
    in
    when (textA /= textB)
        (expectationFailure renderedDiff)


elmFilesOfProject :: FilePath -> Summary.Summary -> IO [FilePath]
elmFilesOfProject projectDirectory summary =
    Elm.getElmFiles projectDirectory (Summary._project summary)


forProjectsElmFilesIt :: [FilePath] -> String -> (FilePath -> Summary.Summary -> FilePath -> IO ()) -> Spec
forProjectsElmFilesIt projectDirectories description test = do
    projects <- runIO $ mapM (\dir -> (,) <$> Elm.parseProject dir <*> pure dir) projectDirectories
    forM_ projects $ uncurry $ \project projectDirectory -> do
        testElmFiles <- runIO (elmFilesOfProject projectDirectory project)
        forM_ testElmFiles $ \testFile ->
            it (description ++ " (" ++ testFile ++ ")") (test projectDirectory project testFile)


testFileTransformTypeCheck :: (FilePath -> Summary.Summary -> FilePath -> Text -> IO Text) -> FilePath -> Summary.Summary -> FilePath -> IO ()
testFileTransformTypeCheck transformFile projectDirectory summary testFile = do
    contentBefore <- Text.readFile testFile
    moduleRendered <- transformFile projectDirectory summary testFile contentBefore

    Exception.bracket_ (Text.writeFile testFile moduleRendered) (Text.writeFile testFile contentBefore) $ do
        Elm.typeCheckFiles projectDirectory >>= \case
            Left exit -> do
                Exit.toStderr exit
                expectationFailure "Fatal failure while trying to type check."

            Right typeCheckAnswer ->
                forM_ (Map.elems typeCheckAnswer) $ \case
                    Right _ ->
                        return ()
                    
                    Left (Elm.TypeCheckFailure path reports) -> do
                        Elm.printReports path reports
                        expectationFailure ("Failed to parse " ++ path ++ " after transforming " ++ testFile)
