{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}

module Language.Elm.ASTSpec (spec) where

-- elm-compiler
import qualified Elm.Package as Pkg
import qualified "elm" Reporting.Report as Report
import qualified Parse.Primitives as ElmParser
import qualified Parse.Primitives.Utf8 as Utf8

-- elm-format
import qualified "elm-format" Box as ElmFormat
import qualified ElmFormat.Render.Box as ElmFormat
import qualified ElmVersion

-- misc
import Data.Text (Text)
-- import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Data.Text.Encoding as Text
import Test.Hspec
import Control.Lens ((<&>))

import qualified Language.Elm as Elm
import qualified Language.Elm.AST as AST
import TestUtil


format :: Text -> Either [Report.Report] Text
format sourceCode =
    AST.renderModule . AST.translateModule
        <$> Elm.parseModule Pkg.dummyName sourceCode


translateModuleIdempodency :: FilePath -> IO ()
translateModuleIdempodency moduleFile = do
    source <- Text.readFile moduleFile

    let runTest = do
            onceFormatted <- format source
            twiceFormatted <- format =<< format source
            return (onceFormatted, twiceFormatted)

    case runTest of
        Left reports -> do
            Elm.printReports moduleFile reports
            expectationFailure "Could not parse a file before or after formatting."

        Right (onceFormatted, twiceFormatted) ->
            (moduleFile <> ", formatted once", onceFormatted)
            `shouldNotBeDifferentTo`
            (moduleFile <> ", formatted twice", twiceFormatted)


elmFormatRender :: String -> Text
elmFormatRender =
    ElmFormat.render . ElmFormat.formatString ElmVersion.Elm_0_19_Upgrade ElmFormat.SString


elmCompilerParse :: Text -> Text
elmCompilerParse =
    (\(Right a) -> a) . ElmParser.run Utf8.string . Text.encodeUtf8


spec :: Spec
spec = do
    describe "translateModule" $ do
        forProjectsElmFilesIt
            [ "test-data/elm-spa-example"
            , "test-data/minimal"
            ]
            "is idempodent"
            (\_ _ -> translateModuleIdempodency)

        forProjectsElmFilesIt
            [ "test-data/elm-spa-example"
            , "test-data/minimal"
            ]
            "should keep well-typed-ness" $ testFileTransformTypeCheck $ \_ summary testFile content -> do
                Elm.parseModuleOrCrash (Elm.summaryPkgName summary) testFile content
                    <&> AST.translateModule
                    <&> AST.renderModule


    describe "javascriptStringDecode" $ do
        it "decodes javascript string literals" $ do
            AST.javascriptStringDecode "\\\\ \\\" \\n \\t \\udbe8\\udcff" `shouldBe` "\\ \" \n \t \56296\56575"

        it "helps translation of string literals from the elm-compiler to elm-format" $ do
            -- interestingly, elm-format replaces \r literals in strings with a unicode escape sequence.
            -- I persume that's going to be fixed in the future maybe, I don't really think its intentional.

            -- Also note, that >2 byte unicode sequences are split up!
            elmFormatRender (AST.javascriptStringDecode (elmCompilerParse "\"asdf' \\\" \\n \\r \\u{10A0FF}\""))
            `shouldBe`
            "\"asdf' \\\" \\n \\u{000D} \\u{DBE8}\\u{DCFF}\"\n"
