{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}

module Language.Elm.ModuleDependenciesSpec where

import Test.Hspec

-- elm-compiler
import qualified Elm.Package as Pkg
import qualified Elm.PerUserCache as Elm

-- misc
-- import Test.Hspec
import Control.Lens
import System.FilePath
import qualified System.Directory
import Control.Monad

import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.ModuleDependencies as ModuleDependencies
import TestUtil


spec :: Spec
spec = do
    describe "pathFromModuleName" $ do
        summary <- runIO $ Elm.parseProject "test-data/minimal"

        it "passes simple golden tests for test-data/minimal" $ do
            ModuleDependencies.pathFromModuleName summary "TestModule"
                `shouldReturn` ModuleDependencies.Local "test-data/minimal/./TestModule.elm"

            ModuleDependencies.pathFromModuleName summary "Dependency"
                `shouldReturn` ModuleDependencies.Local "test-data/minimal/./Dependency.elm"

            let elmCore = Pkg.Package (Pkg.Name "elm" "core") (Pkg.Version 1 0 1)

            elmHome <- Elm.getPackageRoot

            ModuleDependencies.pathFromModuleName summary "Array"
                `shouldReturn` ModuleDependencies.Foreign elmCore (elmHome </> "elm/core/1.0.1/src/Array.elm")

        forProjectsElmFilesIt ["test-data/minimal"] "should not point to non-existing files" $ \_ _ testFile -> do
            module_ <- Elm.parseModuleFromFile Pkg.dummyName testFile

            let
                importedModules =
                    toListOf (Elm.moduleImports . traverse . Elm.importName . Elm.valueOfLocated) module_

                getFilePath = \case
                    ModuleDependencies.Local path ->
                        path

                    ModuleDependencies.Foreign _ path ->
                        path

            elmSources <- mapM (ModuleDependencies.pathFromModuleName summary) importedModules
            nonExistingSources <- filterM (fmap not . System.Directory.doesFileExist . getFilePath) elmSources
            nonExistingSources `shouldBe` []

            -- TODO: Spec for 'localDependencies'!