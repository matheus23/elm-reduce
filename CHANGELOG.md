# Revision history for elm-reduce

## 0.19.0.0 -- 2019-07-09

* First version.

## 0.19.0.1 -- 2019-07-10

* Fixed a bug occuring with an example like in the elm-community/typed-svg package
* Gracefully handling crashes during passes now, including
    - an error message
    - a zip archive with all important information for a bug report
    - a gitlab issue link that populates a bug report with relevant info
* From the library perspective:
    - Refactored passes to use module paths instead of module names
    - Changed ModuleDependencies.localDependencies to produce a graph of module pahts instead of a path of module names

## 0.19.0.2 -- 2019-07-14

* Now minimizing the amount of parsed modules before modules are removed, so that even more .elm files that don't contribute to the actual program can be deleted before they cause elm-reduce trouble.
* Supporting shader expressions now (`[glsl| ... ]` blocks)
* Changing modules from port modules back to normal modules, when all ports are deleted. In future versions the elm compiler would otherwise complain about port modules that don't contain any ports.

## 0.19.0.3 -- 2019-07-15

* Added passes:
    - reduce let definitions / destructurings
    - reduce function definition arguments
