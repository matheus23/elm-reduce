# Elm-reduce

This tool is mainly intended to be used for anyone who wants to produce a bug report for the [elm compiler](https://github.com/elm/compiler). The code you submit for the bug report should be the minimal amount of code to reproduce the compiler bug, but getting there can be quite tricky or tedious.

For a quick start use `elm-reduce --help` or, for a short tutorial read `elm-reduce --tutorial`.

So, say you have a project that reproduces the [`Map.!` bug](https://github.com/elm/compiler/issues/1802), but you have a bigger project and want to produce a minimal test case to report. You can download and install `elm-reduce` and reduce your minimal project like this:

## Download and Install

As of yet, you need to compile this haskell project yourself. You need to download and install [Stack](https://docs.haskellstack.org/en/stable/README/) for this.
Once you installed Stack, you can compile the project:

```bash
$ git clone --recursive https://gitlab.com/matheus23/elm-reduce
$ cd elm-reduce
$ stack install
```

This will take a while, as it will download various dependencies (among them a modified version of the elm compiler) and compile them all locally.

This command should tell you where the executable was installed in. In my case (on linux) this is `/home/philipp/local/bin/elm-reduce`, so make sure to add the corresponding directory to your `PATH`.

## Create a test script

Elm-reduce needs to know whether the current project state can still reproduce the compiler issue. You need to create anything executable that communicates this via the exit code:
* Exit code 0 signales that the project **does** reproduce the bug
* Exit code 1 signales that the project **does not** reproduce the bug

So for our `Map.!`-bug example, this could simply be a bash script looking like this:
```bash
#!/bin/bash
rm -r elm-stuff
EXPECTED_ERR="elm: Map.!: given key is not an element in the map"
elm make src/Main.elm --output=elm.js --debug 2>&1 |
    grep -qF "$EXPECTED_ERR"
```

In this script we pipe the `stdout` and `stderr` of `elm make` into `grep -q`. `grep` with the quite flag `-q` will exit with exit code 0 upon finding the first occurence of a given string, and exit code 1 if it can't find any occurences.

You should test your test script like this:

```bash
$ chmod +x testscript # make sure the test script is executable
$ ./testscript
[...]
$ echo $?
0
```

0 should be output in the project that you want to reproduce the bug with.

## Run Elm-reduce

Now all you need to do is run `elm-reduce` on your project!

```bash
# elm-reduce will add commits to your git project, so a new branch makes sense
# If you project is not in a git repository yet, you need to `git init` and create an initial commit
$ git checkout -b bug-report
$ elm-reduce ./testscript --main-module Main
```

The runtime of Elm-reduce depends heavily on the speed of your test script and the size of your project. You can expect the reduction to take between 10 minutes and 2 hours.

## Go make good bug reports!

I hope that this will increase the amounts of bugs reported and improve report quality. In the future this tool could even be used to build a good Elm compiler **fuzz testing** framework, by reducing the test cases that an Elm source code fuzzer would produce.

This project was implemented in terms of my bachelor's thesis at KIT, you can download it from [here](https://pp.ipd.kit.edu/publication.php?id=pkrueger19bachelorarbeit).
