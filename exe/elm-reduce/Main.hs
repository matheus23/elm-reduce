{-# OPTIONS_GHC -Wall #-}

module Main where

import System.FilePath
import qualified System.Environment as Environment

import qualified CommandLineInterface as CLI
import qualified MainLoop


main :: IO ()
main = do
    options <- CLI.handleArgs MainLoop.defaultPassOrder
    Environment.setEnv "ELM_HOME" (CLI.testDir options </> "elm-home")
    MainLoop.run options
