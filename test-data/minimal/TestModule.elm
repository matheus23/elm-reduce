module TestModule exposing (Model, Msg(..), init, main, update, view)

import Array exposing (Array)
import Basics exposing (..)
import Browser as Browsie
import Char exposing (Char)
import Debug
import Dependency exposing (..)
import Html exposing (..)
import List exposing ((::))
import Maybe exposing (Maybe(..))
import Platform exposing (Program)
import Platform.Cmd as Cmd exposing (Cmd)
import Platform.Sub as Sub exposing (Sub)
import Result exposing (Result(..))
import String exposing (String)
import Tuple


type Msg
    = Msg (Array ())


type alias Model =
    { count : Int }


init : Model
init =
    { count = dependency
    }


view : Model -> Html Msg
view model =
    div
        []
        [ text
            (String.fromInt
                model.count
            )
        ]


update : Msg -> Model -> Model
update msg model =
    let
        i =
            init
    in
    case msg of
        Msg x ->
            always
                i
                "\\ \""


main : Program () Model Msg
main =
    Browsie.sandbox
        { init = init
        , view = view
        , update = update
        }
