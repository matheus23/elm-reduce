* [ ] Figure out binary distribution
* [ ] Improve performance by not immediately re-trying variants which previously failed (after a successful variant in between)
* [ ] Improve how test scripts are written
* [ ] Update to elm 0.19.1! Otherwise people are hitting the "elm.json elm version out of bound" error
* [ ] Change CLI program description. Remove 'interestingness' (unknown concept), add something like 'create SSCCEs'
