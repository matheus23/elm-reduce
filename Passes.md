# 16 Non-destructive

Pass descriptor | Implementation Location
----------------|------------------------
format files | `Reduce.Formatting.reduce`
merge modules | `Reduce.MergeModules.reduceModuleDeps`
delete dead modules | `Reduce.MergeModules.reduceDeadModules`
vendor dependencies | `Reduce.Vendoring.dependencyVendoring`
remove-dead-definitions | `Reduce.Definitions.reduceDead`
remove-dead-types | `Reduce.TypeDeclarations.reduceDead`
reduce-unions | `Reduce.TypeDeclarations.reduceUnions`
convert-unions-alias | `Reduce.TypeDeclarations.reduceUnionsToAliases`
reduce-type-aliases | `Reduce.TypeDeclarations.reduceTypeAliases`
inline-alias | `Reduce.TypeDeclarations.reduceInlineAliases`
reduce-to-subtypes | `Reduce.TypeDeclarations.reduceToSubtypes`
remove-imports | `Reduce.Imports.reduce`
remove dead dependencies | `Reduce.Vendoring.reduceDependencies`
remove lets | `Reduce.Definitions.reduceLetsInModule`
remove arguments | `Reduce.Definitions.reduceArgumentsInModule`
indirect to direct dependencies | `Reduce.Vendoring.indirectDependenciesReduction`

# 3 Destructive

Pass descriptor | Implementation Location
----------------|------------------------
stub-definitions | `Reduce.Stubbing.reduce`
reduce-cases | `Reduce.Definitions.reduceCasesInModule`
reduce-to-subexpressions | `Reduce.Definitions.reduceToSubpartsInModule`
