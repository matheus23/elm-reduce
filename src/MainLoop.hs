{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module MainLoop where

-- Elm compiler
import qualified Elm.Project.Summary as Summary
import qualified Elm.Name as Name

-- misc
import Control.Monad (void, unless, forM_)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Loops (dropWhileM)
import qualified Control.Exception as Exception
import Data.Foldable (traverse_)
import qualified System.Process as Process
import qualified System.Exit as System
import qualified System.Directory as Directory
import System.Timeout
import qualified Data.ByteString as ByteString
import qualified Data.ByteString.Char8 as ByteString.Char8
import Data.Monoid
import qualified Data.Map as Map
import Data.Map (Map)
import Control.Lens ((<&>))
import Data.List.NonEmpty (NonEmpty(..))
import System.FilePath ((</>), (<.>))
import qualified System.FilePath as FilePath
import Data.IORef
import Text.Printf
import qualified Codec.Archive.Zip as Zip
import qualified Network.URI.Encode as URI

import qualified Reduce
import qualified Reduce.Vendoring as Vendoring
import qualified Reduce.Formatting as Formatting
import qualified Reduce.Stubbing as Stubbing
import qualified Reduce.MergeModules as MergeModules
import qualified Reduce.Definitions as Definitions
import qualified Reduce.TypeDeclarations as TypeDeclarations
import qualified Reduce.Imports as Imports
import qualified Language.Elm as Elm
import qualified Language.Elm.ModuleDependencies as ModuleDependencies
import qualified CommandLineInterface as CLI
import qualified Graph


type Pass = Int -> CLI.Options -> IO String -> FilePath -> IO ()


availablePasses :: Map String Pass
availablePasses =
    let
        makePass passName passIO =
            ( passName
            , \fixpointIteration options reductionReport modulePath -> void $ passIO
                (show fixpointIteration <> " " <> passName)
                (CLI.testDir options)
                (testCaseInteresting options)
                reductionReport
                modulePath
            )
    in
    Map.fromList
        [ makePass "remove-dead-definitions"
            Definitions.reduceDead
        , makePass "reduce-cases"
            Definitions.reduceCasesInModule
        , makePass "reduce-lets"
            Definitions.reduceLetsInModule
        , makePass "reduce-arguments"
            Definitions.reduceArgumentsInModule
        , makePass "reduce-to-subexpressions"
            Definitions.reduceToSubpartsInModule
        , makePass "remove-dead-types"
            TypeDeclarations.reduceDead
        , makePass "reduce-unions"
            TypeDeclarations.reduceUnions
        , makePass "reduce-type-aliases"
            TypeDeclarations.reduceTypeAliases
        , makePass "convert-unions-alias"
            TypeDeclarations.reduceUnionsToAliases
        , makePass "reduce-to-subtypes"
            TypeDeclarations.reduceToSubtypes
        , makePass "inline-alias"
            TypeDeclarations.reduceInlineAliases
        , makePass "remove-imports"
            Imports.reduce
        , makePass "stub-definitions"
            Stubbing.reduce
        ]


defaultPassOrder :: NonEmpty String
defaultPassOrder =
    -- reduce dead definitions of main module, cleaning up unused, vendored library functions
    "remove-dead-definitions" :|
    -- stub all definitions that are stubbable
    [ "stub-definitions"
    -- reduce dead definitions of main module again, because stubbing removed lots of function calls
    , "remove-dead-definitions"
    -- reduce dead type declarations
    , "remove-dead-types"
    -- reduce case expressions: Remove whole cases, try to replace matches with wildcards
    , "reduce-cases"
    -- remove let definitions
    , "reduce-lets"
    -- remove functions that are now not referred to anymore by deleted cases or let definitions
    , "remove-dead-definitions"
    -- reduce dead constructors, since lots of stubbed functions don't actually use the constructors anymore
    , "reduce-unions"
    -- reduce dead record fields, since lots of stubbed functions don't actually use these record fields anymore
    , "reduce-type-aliases"
    -- reduce dead declarations, since lots of type dependencies were removed by reducing constructors
    , "remove-dead-types"
    -- convert single-constructor datatypes to aliases, since there should be many of them after reducing unions
    , "convert-unions-alias"
    -- inline aliases
    , "inline-alias"
    -- reduce expressions to their subexpressions
    , "reduce-to-subexpressions"
    -- reduce arguments of definitons before reducing to subtypes to make it possible to reduce the types
    , "reduce-arguments"
    -- reduce types to subtypes. This might pick up some cases, in which expressions are irrelevant to our issues
    , "reduce-to-subtypes"
    -- reduce imports (so future module reductions can be applied)
    , "remove-imports"
    ]


figureOutPass :: String -> IO (String, Pass)
figureOutPass name =
    let
        printError = do
            putStrLn ("Could not find a pass with name " <> show name <> ". See --help for more info.")
            System.exitFailure
    in
    maybe printError (return . ((,) name)) (availablePasses Map.!? name)


-- TODO: All passes kind of work the same and (mostly) have the same arguments. How about they don't directly call Reduce.greedily themselves,
-- but something like IO (Reduce.Reduction x y).
-- Then we would have some form of interpreting these Passes that does all the important stuff:
-- * Reduce.greedily
-- * handlePassException
-- * reductionReport
-- * ? maybe more
-- TODO: Start doing an RIO approach. Maybe simply a type alias for CLI.Options -> IO a
run :: CLI.Options -> IO ()
run options = do
    passes <- traverse figureOutPass (CLI.passOrder options)

    let initialReport = return "[Preparation]"
    let readElmJson = Summary._project <$> Elm.parseProject (CLI.testDir options)

    unless (CLI.noVendoring options) $ do
        -- Reduce elm.json, vendoring
        handlePassException
            options
            "reduce-indirect-dependencies"
            (void $ Vendoring.indirectDependenciesReduction "reduce-indirect-dependencies" (MainLoop.testCaseInteresting options) initialReport (CLI.testDir options) =<< readElmJson)
        handlePassException
            options
            "vendor"
            (void $ Vendoring.dependencyVendoring "vendor" (MainLoop.testCaseInteresting options) initialReport (CLI.testDir options) =<< readElmJson)

    -- we try to delete modules before formatting, as we can delete .elm files that contain syntax errors like this
    -- we only parse modules that are reachable from the main module
    handlePassException
        options
        "remove-dead-modules"
        (void $ MergeModules.reduceDeadModules "remove-dead-modules" (Name.fromString (CLI.mainModule options)) (CLI.testDir options) (testCaseInteresting options) initialReport)

    -- format all files
    handlePassException
        options
        "format"
        (void $ Formatting.reduce "format" (CLI.testDir options) (MainLoop.testCaseInteresting options) initialReport)

    fixpointIterationRef <- newIORef 0
    initialCodeSize <- measureCodeMetrics (CLI.testDir options)
    endSize <- smallestFixpoint (allPassesOnce fixpointIterationRef options (makeReductionReport (CLI.testDir options) initialCodeSize) passes) initialCodeSize

    handlePassException
        options
        "reduce-direct-dependencies"
        (void $ Vendoring.reduceDependencies "reduce-direct-dependencies" (Name.fromString (CLI.mainModule options)) (CLI.testDir options) (testCaseInteresting options) (makeReductionReport (CLI.testDir options) initialCodeSize))

    putStrLn "Could not reduce code size further."
    putStrLn ("Reduced code size: " ++ show endSize ++ " bytes (first invocation: " ++ show initialCodeSize ++ " bytes)")


measureCodeMetrics :: FilePath -> IO Int
measureCodeMetrics projectDir =
    Elm.readProject projectDir
    >>= Elm.getElmFiles projectDir
    <&> ((:) (projectDir </> "elm" <.> "json"))
    >>= traverse (fmap ByteString.length . ByteString.readFile)
    <&> foldMap Sum
    <&> getSum


makeReductionReport :: FilePath -> Int -> IO String
makeReductionReport dir initialCodeSize = do
    currentSize <- measureCodeMetrics dir
    return $
        (printf :: String -> Float -> Int -> String)
            "%.2f%% (%d bytes)"
            (fromIntegral currentSize / fromIntegral initialCodeSize * 100)
            currentSize


-- repeatedly execute f until its not monotone (reducing its parameter) anymore.
smallestFixpoint :: Monad m => m Int -> Int -> m Int
smallestFixpoint f =
    let
        iterateFrom i = do
            i' <- f
            if i <= i' then
                return i
            else
                iterateFrom i'
    in
    iterateFrom


handlePassException :: CLI.Options -> String -> IO () -> IO ()
handlePassException options passName pass =
    let
        projectDir =
            CLI.testDir options

        testscriptPath =
            CLI.testscript options

        bugFileNames =
            map (\i -> "elm-reduce-bug-" <> passName <> "-" <> show @Int i <.> "zip") [0..]

        findBugFile =
            head <$> dropWhileM Directory.doesPathExist bugFileNames

        writeZipByteString path byteString =
            Zip.mkEntrySelector (FilePath.makeValid (FilePath.normalise path))
                >>= Zip.addEntry Zip.BZip2 byteString
        
        writeZipString path str =
            writeZipByteString path (ByteString.Char8.pack str)
    in
    Exception.catch
        pass
        (\(e :: Exception.SomeException) -> do
            putStrLn ("Exception during pass: " <> passName <> ": " <> Exception.displayException e)

            bugFile <- findBugFile
            elmJson <- ByteString.readFile (projectDir </> "elm" <.> "json")
            elmFiles <- Elm.readProject projectDir >>= Elm.getElmFiles projectDir
            testscript <- ByteString.readFile (projectDir </> testscriptPath)

            Zip.createArchive bugFile $ do
                writeZipByteString ("elm" <.> "json") elmJson
                writeZipByteString testscriptPath testscript
                writeZipString ("errormessage" <.> "txt") ("Pass: " <> passName <> "\n" <> Exception.displayException e)
                writeZipString ("commandlinearguments" <.> "txt") (show options)
                forM_ elmFiles $ \elmFile ->
                    writeZipByteString elmFile =<< liftIO (ByteString.readFile (projectDir </> elmFile))

            putStrLn $ unlines
                [ "This might be a bug in Elm-reduce."
                , "An archive with all relevant information from this project has been saved to "
                , ""
                , ("    " <> bugFile)
                , ""
                , "Please create an issue on gitlab. You can use this link:"
                , ""
                , ("    https://gitlab.com/matheus23/elm-reduce/issues/new"
                    <> "?issue[title]=" <> URI.encode (passName <> " pass crashed")
                    <> "&issue[description]=" <> URI.encode
                        (  "Please upload the saved archive to this bug report.\n"
                        <> "The error message was:\n"
                        <> "```\n"
                        <> Exception.displayException e
                        <> "\n```"
                    )
                )
                , ""
                , "Thank you!"
                , ""
                ]
        )


runPasses :: CLI.Options -> IO String -> NonEmpty (String, Pass) -> FilePath -> Int -> IO ()
runPasses options reductionReport passes modulePath fixpointIteration =
    traverse_
        (\(passName, pass) ->
            handlePassException options passName
                (pass fixpointIteration options reductionReport modulePath)
        )
        passes


allPassesOnce :: IORef Int -> CLI.Options -> IO String -> NonEmpty (String, Pass) -> IO Int
allPassesOnce iterationRef options reductionReport passes = do
    let
        CLI.Options{ testDir, mainModule } =
            options

        mainModuleName =
            Name.fromString mainModule

        testCase =
            testCaseInteresting options

    -- Merge modules at the beginning of each run. Previous runs could have introduced new merging opportunities
    handlePassException
        options
        "remove-dead-modules"
        (void $ MergeModules.reduceDeadModules "remove-dead-modules" mainModuleName testDir testCase reductionReport)
    handlePassException
        options
        "merge-modules"
        (void $ MergeModules.reduceModuleDeps "merge-modules" mainModuleName testDir testCase reductionReport)

    localDepGraph <- ModuleDependencies.localDependencies mainModuleName =<< Elm.parseProject testDir
    traverse_
        (\modulePath -> readIORef iterationRef >>= runPasses options reductionReport passes modulePath >> modifyIORef iterationRef (+1))
        (Graph.topologicalSort localDepGraph)

    measureCodeMetrics testDir


testCaseInteresting :: CLI.Options -> IO Reduce.Interestingness
testCaseInteresting CLI.Options{ customTimeout, testDir, testscript } =
    let
        processToTest =
            (Process.proc testscript [])
                { Process.cwd = Just testDir
                }

        handleTimeout = \case
            Just exit ->
                return exit

            -- Indicates timeout
            Nothing -> do
                putStrLn $ "Test case stopped because of timeout (" <> show customTimeout <> "ms)"
                return Reduce.Uninteresting

        runProcess = do
            Process.readCreateProcessWithExitCode processToTest "" >>= \case
                (System.ExitSuccess, _, _) -> return Reduce.Interesting
                (System.ExitFailure _, out, err) -> do
                    putStrLn out
                    putStrLn err
                    return Reduce.Uninteresting
    in
    handleTimeout =<< timeout (customTimeout * 1000) runProcess
