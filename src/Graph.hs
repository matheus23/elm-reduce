{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}

module Graph where

import qualified Data.Graph.Inductive as FGL

import qualified Data.Map as Map
import Data.Map (Map)
import qualified Data.Set as Set
import Data.Set (Set)
import qualified Data.Maybe as Maybe
import Data.Function ((&))
import Control.Monad (void)


type Graph a = Map a [a]


dotfile :: Show a => Graph a -> String
dotfile dependencyGraph =
    let
        quote =
            show . show

        ornaments ls =
            ["digraph dependencies {"]
            <> ls
            <> ["}"]

        flatten refs =
            [ quote nodeName <> " -> " <> quote refName <> ";"
            | (nodeName, nodeRefs) <- refs
            , refName <- nodeRefs
            ]
    in
    dependencyGraph
        & Map.toList
        & flatten
        & ornaments
        & unlines


{-| Given a directed graph as adjacency map, this builds the graph with all edges reversed -}
reversed :: Ord a => Graph a -> Graph a
reversed =
    let
        combineMaps =
            Map.unionsWith mappend

        {-| From the view of a single node with references to other nodes,
            this builds the graph with all edges reversed
         -}
        reverseOneToN source references =
            combineMaps
                ( Map.singleton source [] -- so that we include unreferenced entries
                : map (reverseOneToOne source) references
                )

        {-| From the view of a single node connected to another node,
            this builds the graph with all edges reversed
        -}
        reverseOneToOne source reference =
            Map.singleton reference [source]
    in
    combineMaps
    . map (uncurry reverseOneToN)
    . Map.toList


analizeReferences :: Ord a => ((a, [a]) -> Maybe b) -> Graph a -> [b]
analizeReferences analizeDependency =
    Maybe.catMaybes . map analizeDependency . Map.toList . reversed


referencedOnce :: Ord a => Graph a -> [(a, a)]
referencedOnce =
    analizeReferences $ \ref -> do
        -- using MonadFail for Maybe
        (referenced, [soleReferrer]) <- return ref
        return (soleReferrer, referenced)


neverReferenced :: Ord a => Graph a -> [a]
neverReferenced =
    analizeReferences $ \ref -> do
        -- using MonadFail for Maybe
        (unreferenced, []) <- return ref
        return unreferenced


topologicalSort :: Ord a => Graph a -> [a]
topologicalSort =
    FGL.topsort' . toFglGraph @FGL.Gr


toFglGraph :: (FGL.DynGraph g, Ord a) => Graph a -> g a ()
toFglGraph graph =
    let
        insertMapNodes node connected = do
            void $ FGL.insMapNodeM node
            traverse
                (\connection -> do
                    void $ FGL.insMapNodeM connection
                    void $ FGL.insMapEdgeM (node, connection, ())
                )
                connected
    in
    FGL.run_ FGL.empty (Map.traverseWithKey insertMapNodes graph)


reachableFrom :: Ord a => a -> Graph a -> Set a
reachableFrom =
    let
        reachableFromWith alreadyReached from references =
            let
                referred =
                    Map.findWithDefault [] from references

                alreadyReachedNow =
                    Set.insert from alreadyReached

                referredUnreached =
                    Set.fromList referred Set.\\ alreadyReachedNow
            in
            referredUnreached
                & Set.toList
                & map (\reference -> reachableFromWith alreadyReachedNow reference references)
                & Set.unions
                & Set.insert from
    in
    reachableFromWith Set.empty
