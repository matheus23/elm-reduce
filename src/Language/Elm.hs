{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
module Language.Elm where

import Control.Concurrent (forkIO)
import Control.Concurrent.MVar (MVar, newEmptyMVar, newMVar, putMVar, readMVar, takeMVar)
import Control.Concurrent.Chan (Chan, newChan, readChan, writeChan, getChanContents)
import Control.Monad.IO.Class
import Control.Monad
import System.FilePath ((</>))
import qualified System.FilePath.Glob as Glob
import qualified Data.Map as Map
import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as T
import Data.Maybe
import qualified System.IO

-- ELM COMPILER MODULES
import qualified Compile
import qualified Parse.Parse
import qualified Canonicalize.Module
import qualified Elm.Compiler
import qualified Elm.Compiler.Module
import qualified Elm.Interface
import qualified Elm.Package
import qualified Elm.Project.Json
import qualified Elm.Project.Summary
import qualified Elm.Name
import qualified AST.Optimized
import qualified AST.Valid
import qualified File.Args
import qualified File.Compile
import qualified File.Crawl
import qualified File.Plan
import qualified Reporting.Progress.Json
import qualified Reporting.Progress.Terminal
import qualified Reporting.Doc
import qualified Reporting.Error
import qualified "elm" Reporting.Error.Syntax
import qualified Reporting.Render.Code
import qualified Reporting.Report
import qualified Reporting.Progress
import qualified Reporting.Task
import qualified Reporting.Warning
import qualified Reporting.Result
import qualified Reporting.Exit.Crawl
import qualified Reporting.Exit
import qualified Reporting.Render.Type.Localizer
import qualified "elm" Reporting.Region -- would conflict with elm-format's Reporting.Region
import qualified "elm" Reporting.Annotation
import qualified Stuff.Verify

import Language.Elm.OrphanInstances


data TypeCheckSuccess = TypeCheckSuccess
    { interface :: Elm.Interface.Interface
    }
    deriving Show

data TypeCheckFailure = TypeCheckFailure
    { sourcePath :: FilePath
    , errors :: [Reporting.Report.Report]
    }

type CheckingAnswer = Either TypeCheckFailure TypeCheckSuccess
type CheckingAnswers = Map.Map Elm.Compiler.Module.Raw CheckingAnswer

-- Use Elm Compiler to typecheck files
-- root is where elm.json lives
typeCheckFiles :: MonadIO m => FilePath -> m (Either Reporting.Exit.Exit CheckingAnswers)
typeCheckFiles root = do
    reporter <- liftIO Reporting.Progress.Terminal.create
    liftIO $ Reporting.Task.tryWithError reporter $ do
        project <- Elm.Project.Json.read (root </> "elm.json")
        Elm.Project.Json.check project
        summary <- Stuff.Verify.verify root project
        -- get files from the  project
        files <- liftIO $ getElmFiles root project

        args <- File.Args.fromPaths summary files
        graph <- File.Crawl.crawl summary args
        (dirtyModules, interfaces) <- File.Plan.plan Nothing summary graph

        answers <- liftIO $ do
            mvar <- newEmptyMVar
            iMVar <- newMVar interfaces
            answerMVars <- Map.traverseWithKey (compileModule project mvar iMVar) dirtyModules
            putMVar mvar answerMVars
            traverse readMVar answerMVars

        return (Map.mapMaybe computeInterfaceOrErrors answers)

computeInterfaceOrErrors :: File.Compile.Answer -> Maybe CheckingAnswer
computeInterfaceOrErrors answer =
    case answer of
        -- Blocked cases are usually just ignored in the compiler, see builder/src/File/Artifacts.hs#L102 in elm-compiler
        File.Compile.Blocked ->
            Nothing

        File.Compile.Bad path timeStamp source errors ->
            Just (Left (TypeCheckFailure path (compilerErrorsToReports source errors)))

        File.Compile.Good (Elm.Compiler.Artifacts interface output documentation) ->
            Just (Right (TypeCheckSuccess interface))

compilerErrorsToReports :: BS.ByteString -> [Elm.Compiler.Error] -> [Reporting.Report.Report]
compilerErrorsToReports sourceRaw errors =
    let
        source = Reporting.Render.Code.toSource (T.decodeUtf8 sourceRaw)
    in
        concatMap (Reporting.Error.toReports source) errors

compileModule
    :: Elm.Project.Json.Project
    -> MVar (Map.Map Elm.Compiler.Module.Raw (MVar File.Compile.Answer))
    -> MVar Elm.Interface.Interfaces
    -> Elm.Compiler.Module.Raw
    -> File.Plan.Info
    -> IO (MVar File.Compile.Answer)
compileModule project answersMVar ifacesMVar name info = do
    mvar <- newEmptyMVar
    void $ forkIO $
        do  answers <- readMVar answersMVar
            blocked <- File.Compile.isBlocked answers info
            if blocked
            then putMVar mvar File.Compile.Blocked
            else
                do  let pkg = Elm.Project.Json.getName project
                    let imports = File.Compile.makeImports project info
                    ifaces <- readMVar ifacesMVar
                    let source = File.Plan._src info
                    case customCompile pkg imports ifaces source of
                        (_warnings, Left errors) -> do
                            let time = File.Plan._time info
                            let path = File.Plan._path info
                            putMVar mvar (File.Compile.Bad path time source errors)

                        (_warnings, Right result@(Compile.Artifacts elmi _ _)) -> do
                            let canonicalName = Elm.Compiler.Module.Canonical pkg name
                            lock <- takeMVar ifacesMVar
                            putMVar ifacesMVar (Map.insert canonicalName elmi lock)
                            putMVar mvar (File.Compile.Good result)

    return mvar

customCompile
    :: Elm.Package.Name
    -> Map.Map Elm.Compiler.Module.Raw Elm.Compiler.Module.Canonical
    -> Map.Map Elm.Compiler.Module.Canonical Elm.Compiler.Module.Interface
    -> BS.ByteString
    -> ([Reporting.Warning.Warning], Either [Reporting.Error.Error] Elm.Compiler.Artifacts)
customCompile pkg importDict interfaces source =
    Reporting.Result.run $ do
        valid <- Reporting.Result.mapError Reporting.Error.Syntax $
            Parse.Parse.program pkg source

        canonical <- Reporting.Result.mapError Reporting.Error.Canonicalize $
            Canonicalize.Module.canonicalize pkg importDict interfaces valid

        let localizer = Reporting.Render.Type.Localizer.fromModule valid

        annotations <-
            Compile.runTypeInference localizer canonical

        () <-
            Compile.exhaustivenessCheck canonical

        -- we don't actually run any code generation

        Reporting.Result.ok $
            Compile.Artifacts
                { Compile._elmi = Elm.Interface.fromModule annotations canonical
                -- we have to fake this, so compileModule can store artifacts in File.Compile.Good
                , Compile._elmo = AST.Optimized.Graph Map.empty Map.empty Map.empty
                , Compile._docs = Nothing
                }


-- Get all elm files given in an elm.json ([] for a package, all elm files for an application)
getElmFiles :: FilePath -> Elm.Project.Json.Project -> IO [FilePath]
getElmFiles rootPath summary = case summary of
    Elm.Project.Json.App app -> do
        let dirs = map (rootPath </>) (Elm.Project.Json._app_source_dirs app)
        elmFiles <- mapM (Glob.globDir1 (Glob.compile "**/*.elm")) dirs
        return (concat elmFiles)
    Elm.Project.Json.Pkg package -> return [] -- TODO Pkg support!


-- TODO: Error handling should be improved for those two functions
parseProject :: FilePath -> IO Elm.Project.Summary.Summary
parseProject root = do
    reporter <- Reporting.Progress.Terminal.create -- create a reporter that reports to terminal
    (Just summary) <- Reporting.Task.try reporter $ do
        project <- Elm.Project.Json.read (root </> "elm.json")
        Elm.Project.Json.check project
        Stuff.Verify.verify root project
    return summary


readProject :: FilePath -> IO Elm.Project.Json.Project
readProject root = do
    reporter <- Reporting.Progress.Terminal.create -- create a reporter that reports to terminal
    (Just project) <- Reporting.Task.try reporter (Elm.Project.Json.read (root </> "elm.json"))
    return project


parseModuleFromFile :: Elm.Package.Name -> FilePath -> IO AST.Valid.Module
parseModuleFromFile pkgName path =
    parseModuleOrCrash pkgName path =<< T.readFile path


parseModuleOrCrash :: Elm.Package.Name -> String -> T.Text -> IO AST.Valid.Module
parseModuleOrCrash pkgName modulePathForErrors moduleText =
    case parseModule pkgName moduleText of
        Right validModule ->
            return validModule

        Left reports -> do
            printReports modulePathForErrors reports
            error ("Failed to parse module at " ++ show modulePathForErrors ++ " due to the errors above.")


parseModule :: Elm.Package.Name -> T.Text -> Either [Reporting.Report.Report] AST.Valid.Module
parseModule pkgName moduleText =
    case Reporting.Result.run (Parse.Parse.program pkgName (T.encodeUtf8 moduleText)) of
        (_, Right validModule) ->
            Right validModule

        (_, Left errors) ->
            let
                source =
                    Reporting.Render.Code.toSource moduleText
            in
            Left (map (Reporting.Error.Syntax.toReport source) errors)


printReports :: FilePath -> [Reporting.Report.Report] -> IO ()
printReports sourceFilePath =
    mapM_ (Reporting.Doc.toAnsi System.IO.stdout . Reporting.Report.toDoc sourceFilePath)


dummyLocation :: a -> Reporting.Annotation.Located a
dummyLocation = Reporting.Annotation.at (Reporting.Region.Position 0 0) (Reporting.Region.Position 0 0)


dummyRegion :: Reporting.Region.Region
dummyRegion = Reporting.Region.Region (Reporting.Region.Position 0 0) (Reporting.Region.Position 0 0)


summaryPkgName :: Elm.Project.Summary.Summary -> Elm.Package.Name
summaryPkgName =
    Elm.Project.Json.getName . Elm.Project.Summary._project
