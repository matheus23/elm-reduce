{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}

module Language.Elm.Traverse where

-- Elm compiler
import qualified AST.Source as Src
import qualified Elm.Name as Name

-- misc
import qualified Data.Set as Set
import Data.Set (Set)
import Control.Lens hiding ((<.>))

import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.Traverse.Pattern as Pattern
import qualified Language.Elm.Traverse.Expr as Expr

type LocallyBound =
    Set Name.Name


withLocallyBound :: (LocallyBound -> Expr.F a -> b) -> Expr.F (LocallyBound -> a) -> LocallyBound -> b
withLocallyBound algebra expr locallyBound = case expr of
    Expr.Lambda arguments body ->
        algebra locallyBound (Expr.Lambda arguments (body (boundByPatterns arguments <> locallyBound)))

    Expr.Case ofExpr cases ->
        let
            handleCase (pattern, body) =
                (pattern, body (boundByPattern pattern <> locallyBound))
        in
        algebra locallyBound (Expr.Case (ofExpr locallyBound) (map handleCase cases))

    Expr.Let definitions in_ ->
        let
            allPatterns =
                toListOf (traverse . Expr._DefineF . Elm.letDefArguments . traverse) definitions
                ++ toListOf (traverse . Expr._DestructF . Elm.letDestructPattern) definitions

            allIdentifierNames =
                definitions
                    & toListOf (traverse . Expr._DefineF . Elm.letDefName . Elm.valueOfLocated)

            definedNames =
                boundByPatterns allPatterns
                <> Set.fromList allIdentifierNames

            locallyBoundInside =
                locallyBound <> definedNames
        in
        algebra locallyBound (Expr.Let (map (fmap ($ locallyBoundInside)) definitions) (in_ locallyBoundInside))

    _ ->
        algebra locallyBound (fmap ($ locallyBound) expr)


boundByPatterns :: [Src.Pattern] -> Set Name.Name
boundByPatterns = Set.unions . map boundByPattern


boundByPattern :: Src.Pattern -> Set Name.Name
boundByPattern =
    Pattern.fold (boundInPatternAlg . view Elm.valueOfLocated)


boundInPatternAlg :: Pattern.F (Set Name.Name) -> Set Name.Name
boundInPatternAlg = Pattern.collectWith $ \case
    Pattern.Var name ->
        Set.singleton name

    Pattern.Record fields ->
        Set.fromList (map (view Elm.valueOfLocated) fields)

    Pattern.Alias _ name ->
        Set.singleton (view Elm.valueOfLocated name)

    _ ->
        mempty
