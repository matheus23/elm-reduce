-- shadowing: name shadowing is actually really useful here to not accidentally write unlawful lenses
{-# OPTIONS_GHC -Wall -fno-warn-name-shadowing -fno-warn-orphans #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE StandaloneDeriving #-}

module Language.Elm.Lenses where

import Control.Lens

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified "elm" Reporting.Annotation as Ann
import qualified "elm" Reporting.Region as R
import qualified Elm.Project.Json as Json
import qualified Elm.Package as Pkg
import qualified Elm.Name as Name
import qualified Elm.Project.Summary as Summary
import Data.Text (Text)
import Data.Map (Map)


linesOf :: R.Region -> Int
linesOf (R.Region (R.Position startLine _) (R.Position endLine _)) =
    endLine - startLine + 1


declarationBody :: Simple Lens Elm.Decl Elm.Expr
declarationBody f (Elm.Decl name patterns body typeAnnotation) =
    (\body -> Elm.Decl name patterns body typeAnnotation) <$> f body


declarationName :: Simple Lens Elm.Decl (Ann.Located Name.Name)
declarationName f (Elm.Decl name patterns body typeAnnotation) =
    (\name -> Elm.Decl name patterns body typeAnnotation) <$> f name


declarationArguments :: Simple Lens Elm.Decl [Src.Pattern]
declarationArguments f (Elm.Decl name arguments body typeAnnotation) =
    (\arguments -> Elm.Decl name arguments body typeAnnotation) <$> f arguments


declarationTypeAnnotation :: Simple Lens Elm.Decl (Maybe Src.Type)
declarationTypeAnnotation f (Elm.Decl name patterns body typeAnnotation) =
    (\typeAnnotation -> Elm.Decl name patterns body typeAnnotation) <$> f typeAnnotation


nameText :: Simple Lens Name.Name Text
nameText f name =
    Name.fromText <$> f (Name.toText name)


moduleName :: Simple Lens Elm.Module Name.Name
moduleName f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\name -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f name)


moduleDeclarations :: Simple Lens Elm.Module [Ann.Located Elm.Decl]
moduleDeclarations f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\decls -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f decls)


moduleAliases :: Simple Lens Elm.Module [Elm.Alias]
moduleAliases f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\aliases -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f aliases)


moduleUnions :: Simple Lens Elm.Module [Elm.Union]
moduleUnions f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\unions -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f unions)


moduleOperators :: Simple Lens Elm.Module [Elm.Binop]
moduleOperators f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\binops -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f binops)


moduleImports :: Simple Lens Elm.Module [Src.Import]
moduleImports f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\imports -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f imports)


moduleExports :: Simple Lens Elm.Module (Ann.Located Src.Exposing)
moduleExports f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\exports -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f exports)


moduleEffects :: Simple Lens Elm.Module Elm.Effects
moduleEffects f (Elm.Module name overview docs exports imports decls unions aliases binops effects) =
    fmap (\effects -> Elm.Module name overview docs exports imports decls unions aliases binops effects)
        (f effects)


unionRegion :: Simple Lens Elm.Union R.Region
unionRegion f (Elm.Union region name freeVars constructors) =
    fmap (\region -> Elm.Union region name freeVars constructors)
        (f region)


unionConstructors :: Simple Lens Elm.Union [(Ann.Located Name.Name, [Src.Type])]
unionConstructors f (Elm.Union region name freeVars constructors) =
    fmap (\constructors -> Elm.Union region name freeVars constructors)
        (f constructors)


constructorFields :: Simple Lens (Ann.Located Name.Name, [Src.Type]) [Src.Type]
constructorFields = _2


unionName :: Simple Lens Elm.Union (Ann.Located Name.Name)
unionName f (Elm.Union region name freeVars constructors) =
    fmap (\name -> Elm.Union region name freeVars constructors)
        (f name)


aliasRegion :: Simple Lens Elm.Alias R.Region
aliasRegion f (Elm.Alias region name freeVars type_) =
    fmap (\region -> Elm.Alias region name freeVars type_) (f region)


aliasName :: Simple Lens Elm.Alias (Ann.Located Name.Name)
aliasName f (Elm.Alias region name freeVars type_) =
    fmap (\name -> Elm.Alias region name freeVars type_) (f name)


aliasType :: Simple Lens Elm.Alias Src.Type
aliasType f (Elm.Alias region name freeVars type_) =
    fmap (\type_ -> Elm.Alias region name freeVars type_) (f type_)


aliasTypeParams :: Simple Lens Elm.Alias [Ann.Located Name.Name]
aliasTypeParams f (Elm.Alias region name freeVars type_) =
    fmap (\freeVars -> Elm.Alias region name freeVars type_) (f freeVars)


binopName :: Simple Lens Elm.Binop Name.Name
binopName f (Elm.Binop name associativity precedence functionName) =
    (\name -> Elm.Binop name associativity precedence functionName) <$> f name


valueOfLocated :: Lens (Ann.Located a) (Ann.Located b) a b
valueOfLocated f (Ann.At region val) =
    Ann.At region <$> f val


regionOfLocated :: Simple Lens (Ann.Located a) R.Region
regionOfLocated f (Ann.At region val) =
    flip Ann.At val <$> f region


importName :: Simple Lens Src.Import (Ann.Located Name.Name)
importName f (Src.Import name alias exposing) =
    (\name -> Src.Import name alias exposing) <$> f name


importAlias :: Simple Lens Src.Import (Maybe Name.Name)
importAlias f (Src.Import name alias exposing) =
    (\alias -> Src.Import name alias exposing) <$> f alias


data LetDefinition a
    = LetDefinition
    { _letDefRegion :: R.Region
    , _letDefName :: Ann.Located Name.Name
    , _letDefArguments :: [Src.Pattern]
    , _letDefBody :: a
    , _letDefTypeAnnotation :: Maybe Src.Type
    }
    deriving (Functor, Foldable, Traversable)

makeLenses ''LetDefinition

data LetDestruct a
    = LetDestruct
    { _letDestructRegion :: R.Region
    , _letDestructPattern :: Src.Pattern
    , _letDestructBody :: a
    }
    deriving (Functor, Foldable, Traversable)

makeLenses ''LetDestruct


_Define :: Simple Prism Elm.Def (LetDefinition Elm.Expr)
_Define =
    prism'
        (\(LetDefinition region name arguments in_ annotation) -> Elm.Define region name arguments in_ annotation)
        (\case
            Elm.Define region name arguments in_ annotation ->
                Just (LetDefinition region name arguments in_ annotation)

            Elm.Destruct{} ->
                Nothing
        )


_Destruct :: Simple Prism Elm.Def (LetDestruct Elm.Expr)
_Destruct =
    prism'
        (\(LetDestruct region name pattern) -> Elm.Destruct region name pattern)
        (\case
            Elm.Destruct region name pattern ->
                Just (LetDestruct region name pattern)

            Elm.Define{} ->
                Nothing
        )


_Ports :: Simple Prism Elm.Effects [Elm.Port]
_Ports = prism' Elm.Ports $ \case
    Elm.Ports ports ->
        Just ports

    _ ->
        Nothing


portName :: Simple Lens Elm.Port (Ann.Located Name.Name)
portName f (Elm.Port name type_) =
    (\name -> Elm.Port name type_) <$> f name


portType :: Simple Lens Elm.Port Src.Type
portType f (Elm.Port name type_) =
    (\type_ -> Elm.Port name type_) <$> f type_


_Explicit :: Simple Prism Src.Exposing [Ann.Located Src.Exposed]
_Explicit = prism' Src.Explicit $ \case
    Src.Explicit exposed ->
        Just exposed

    Src.Open ->
        Nothing


_Lower :: Simple Prism Src.Exposed Name.Name
_Lower = prism' Src.Lower $ \case
    Src.Lower lower ->
        Just lower

    _ ->
        Nothing

_Upper :: Simple Prism Src.Exposed (Name.Name, Src.Privacy)
_Upper = prism' (uncurry Src.Upper) $ \case
    Src.Upper name privacy ->
        Just (name, privacy)

    _ ->
        Nothing

_Operator :: Simple Prism Src.Exposed Name.Name
_Operator = prism' Src.Operator $ \case
    Src.Operator name ->
        Just name

    _ ->
        Nothing



-- Src.Type prisms


data TRecord a = TRecord
    { _recordTypeFields :: [(Ann.Located Name.Name, a)]
    , _recordTypeExtension :: Maybe (Ann.Located Name.Name)
    }
    deriving Functor

makeLenses ''TRecord

_TRecord :: Simple Prism Src.Type_ (TRecord Src.Type)
_TRecord = prism'
    (\(TRecord fields extension) -> Src.TRecord fields extension)
    (\case
        Src.TRecord fields extension ->
            Just (TRecord fields extension)

        _ ->
            Nothing
    )

-- Json.Project

summaryProject :: Simple Lens Summary.Summary Json.Project
summaryProject f summary =
    (\jsonProject -> summary { Summary._project = jsonProject }) <$> f (Summary._project summary)


_AppInfo :: Simple Prism Json.Project Json.AppInfo
_AppInfo = prism' Json.App $ \case
    Json.App appInfo ->
        Just appInfo

    _ ->
        Nothing

_PkgInfo :: Simple Prism Json.Project Json.PkgInfo
_PkgInfo = prism' Json.Pkg $ \case
    Json.Pkg pkgInfo ->
        Just pkgInfo

    _ ->
        Nothing

appVersion :: Simple Lens Json.AppInfo Pkg.Version
appVersion f (Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) =
    fmap (\version -> Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) (f version)


appSourceDirs :: Simple Lens Json.AppInfo [FilePath]
appSourceDirs f (Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) =
    fmap (\sourceDirs -> Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) (f sourceDirs)


appDepsDirect :: Simple Lens Json.AppInfo (Map Pkg.Name Pkg.Version)
appDepsDirect f (Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) =
    fmap (\depsDirect -> Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) (f depsDirect)


appDepsTransitive :: Simple Lens Json.AppInfo (Map Pkg.Name Pkg.Version)
appDepsTransitive f (Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) =
    fmap (\depsTransitive -> Json.AppInfo version sourceDirs depsDirect depsTransitive testDirect testTransitive) (f depsTransitive)

