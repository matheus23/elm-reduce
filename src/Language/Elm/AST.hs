{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Language.Elm.AST where

-- Elm format
import qualified AST.Module as ElmFormat
import qualified AST.Declaration as ElmFormat
import qualified AST.Pattern as ElmFormat.Pattern
import qualified AST.Expression as ElmFormat
import qualified AST.V0_16 as ElmFormat
import qualified AST.Variable as ElmFormat
import qualified ElmVersion as ElmFormat.ElmVersion
import qualified ElmFormat.Render.Text as ElmFormat
import qualified ElmFormat.Render.Box as ElmFormat
import qualified "elm-format" Reporting.Annotation as ElmFormat
import qualified "elm-format" Reporting.Region as ElmFormat

-- Elm compiler
import qualified AST.Valid as Elm
import qualified Elm.Name as Name
import qualified AST.Source as Src
import qualified "elm" Reporting.Annotation as Ann

-- misc
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Data.Maybe
import qualified Numeric
import Data.Function
import qualified Data.Char as Char

import Language.Elm.OrphanInstances ()


-- We ignore documentations, binary operations and effects, since they won't appear in client code
-- (especially since elm 0.19)
translateModule :: Elm.Module -> ElmFormat.Module
translateModule (Elm.Module name _ _ exports imports declarations unions aliases _ effects) =
    let
        translateHeader =
            ElmFormat.Header
                { srcTag = translateEffects effects
                , name = uncommented [uppercase (Name.toString name)]
                -- this is used for effect modules, which are only restricted to internal elm packages right now.
                -- TODO: Translate effect modules?
                , moduleSettings = Nothing
                , exports = uncommentedKeyword (translateExposing (Ann.toValue exports))
                }

        translateEntry =
            ElmFormat.Entry . unlocated
    in
    ElmFormat.Module
        { initialComments = []
        , header = translateHeader
        , docs = unlocated Nothing
        , imports = ([], translateImports imports)
        , body =
            -- reverses needed to preserve order of unions, aliases and declarations
            map (translateEntry . translateUnion) (reverse unions)
            ++ map (translateEntry . translateTypeAlias) (reverse aliases)
            ++ concatMap (map translateEntry . translateDeclaration . Ann.toValue) (reverse declarations)
            ++ case effects of
                Elm.Ports ports ->
                    map (translateEntry . translatePort) (reverse ports)

                _ ->
                    []
        }


translateDeclaration :: Elm.Decl -> [ElmFormat.Declaration]
translateDeclaration (Elm.Decl locatedName patterns body typeAnnotation) =
    maybeToList (fmap (translateTypeAnnotation locatedName) typeAnnotation)
    ++ [translateFunctionBody locatedName patterns body]


translateFunctionBody :: Ann.Located Name.Name -> [Src.Pattern] -> Elm.Expr -> ElmFormat.Declaration
translateFunctionBody locatedName parameters body =
    ElmFormat.Definition
        (unlocated (ElmFormat.Pattern.VarPattern (lowercase (Name.toString (Ann.toValue locatedName)))))
        (map (unprecommented . translatePattern) parameters)
        []
        (translateExpression body)


translateExpression :: Elm.Expr -> ElmFormat.Expr
translateExpression locatedExpr =
    unlocated $
        case Ann.toValue locatedExpr of
            Elm.Chr text ->
                ElmFormat.Literal (translateLiteralChar text)

            Elm.Str text ->
                ElmFormat.Literal (translateLiteralString text)

            Elm.Int int ->
                ElmFormat.Literal (translateLiteralInt int)

            Elm.Float double ->
                ElmFormat.Literal (translateLiteralFloat double)

            Elm.Var varType name ->
                ElmFormat.VarExpr (translateVarTypeRef varType [] name)

            Elm.VarQual varType qualifier name ->
                ElmFormat.VarExpr (translateVarTypeRef varType (translateDottedName qualifier) name)

            Elm.List expressions ->
                ElmFormat.ExplicitList (uncommentedSequence (map translateExpression expressions)) [] (ElmFormat.ForceMultiline True)

            Elm.Op name ->
                ElmFormat.VarExpr (ElmFormat.OpRef (ElmFormat.SymbolIdentifier (Name.toString name)))

            Elm.Negate expr ->
                ElmFormat.Unary ElmFormat.Negative (translateExpression expr)

            -- A little bit hairy
            Elm.Binops ((firstExpression, firstOperator):moreOpExpressions) lastExpression ->
                let
                    uncomment operator expr =
                        ([], operator, [], expr)

                    operators =
                        firstOperator : map snd moreOpExpressions

                    expressions =
                        map fst moreOpExpressions ++ [lastExpression]

                    translateOperator =
                        ElmFormat.OpRef . ElmFormat.SymbolIdentifier . Name.toString . Ann.toValue
                in
                ElmFormat.Binops (translateExpression firstExpression) (zipWith uncomment (map translateOperator operators) (map translateExpression expressions)) True

            Elm.Binops [] _ ->
                error "Invalid AST: Binary operations node without operators"

            Elm.Lambda patterns body ->
                ElmFormat.Lambda (map (unprecommented . translatePattern) patterns) [] (translateExpression body) True

            Elm.Call function arguments ->
                ElmFormat.App (translateExpression function) (map (unprecommented . translateExpression) arguments) ElmFormat.FASplitFirst

            Elm.If (firstIfThen : moreIfThens) elseExpr ->
                let
                    translateIfCondition (conditionExpr, thenExpr) =
                        ( uncommented (translateExpression conditionExpr)
                        , uncommented (translateExpression thenExpr)
                        )
                in
                ElmFormat.If
                    (translateIfCondition firstIfThen)
                    (map (unprecommented . translateIfCondition) moreIfThens)
                    (unprecommented (translateExpression elseExpr))

            Elm.If [] _ ->
                error "Invalid AST: If node with only an else branch"

            Elm.Let definitions inExpr ->
                let
                    translateAnnotation name typeAnnotation =
                        ElmFormat.LetAnnotation
                            (ElmFormat.VarRef [] (lowercase (Name.toString name)), [])
                            ([], translateType typeAnnotation)

                    translateLetDefinition = \case
                        Elm.Define _ locatedName argumentPatterns bodyExpr typeAnnotation ->
                            maybeToList (fmap (translateAnnotation (Ann.toValue locatedName)) typeAnnotation)
                                ++
                                    [ ElmFormat.LetDefinition
                                        (unlocated (ElmFormat.Pattern.VarPattern (lowercase (Name.toString (Ann.toValue locatedName)))))
                                        (map (unprecommented . translatePattern) argumentPatterns)
                                        []
                                        (translateExpression bodyExpr)
                                    ]

                        Elm.Destruct _ pattern expr ->
                            [ ElmFormat.LetDefinition (translatePattern pattern) [] [] (translateExpression expr) ]
                in
                ElmFormat.Let (concatMap translateLetDefinition definitions) [] (translateExpression inExpr)

            Elm.Case ofExpression cases ->
                let
                    translateCase (pattern, expression) =
                        (uncommented (translatePattern pattern), unprecommented (translateExpression expression))
                in
                ElmFormat.Case
                    (uncommented (translateExpression ofExpression), False) -- False: Whether case and of should be on the same line
                    (map translateCase cases)

            Elm.Accessor name ->
                ElmFormat.AccessFunction (lowercase (Name.toString name))

            Elm.Access ofExpression accessor ->
                ElmFormat.Access (translateExpression ofExpression) (lowercase (Name.toString (Ann.toValue accessor)))

            Elm.Update identifierName updateFields ->
                let
                    translateUpdateField (fieldName, fieldExpression) =
                        translatePair fieldName (translateExpression fieldExpression)
                in
                ElmFormat.Record
                    { base = Just (uncommented (lowercase (Name.toString (Ann.toValue identifierName))))
                    , fields = uncommentedSequence (map translateUpdateField updateFields)
                    , trailingComments = []
                    , forceMultiline = ElmFormat.ForceMultiline True
                    }

            Elm.Record fields ->
                let
                    translateField (fieldName, fieldExpression) =
                        translatePair fieldName (translateExpression fieldExpression)
                in
                ElmFormat.Record
                    { base = Nothing
                    , fields = uncommentedSequence (map translateField fields)
                    , trailingComments = []
                    , forceMultiline = ElmFormat.ForceMultiline True
                    }

            Elm.Unit ->
                ElmFormat.Unit []

            Elm.Tuple fstExpr sndExpr moreExprs ->
                ElmFormat.Tuple (map (uncommented . translateExpression) (fstExpr : sndExpr : moreExprs)) False

            Elm.Shader _ source _ ->
                ElmFormat.GLShader (Text.unpack (Text.replace "\\n" "\n" source))


translateVarTypeRef :: Src.VarType -> [ElmFormat.UppercaseIdentifier] -> Name.Name -> ElmFormat.Ref
translateVarTypeRef varType qualifier name =
    case varType of
        Src.Value ->
            ElmFormat.VarRef qualifier (lowercase (Name.toString name))

        Src.Ctor ->
            ElmFormat.TagRef qualifier (uppercase (Name.toString name))


translatePattern :: Src.Pattern -> ElmFormat.Pattern.Pattern
translatePattern locatedPattern =
    unlocated $
        case Ann.toValue locatedPattern of
            Src.PAnything ->
                ElmFormat.Pattern.Anything

            Src.PVar name ->
                ElmFormat.Pattern.VarPattern (lowercase (Name.toString name))

            Src.PRecord locatedFields ->
                ElmFormat.Pattern.Record (map (uncommented . lowercase . Name.toString . Ann.toValue) (reverse locatedFields))

            Src.PAlias pattern locatedAlias ->
                ElmFormat.Pattern.Alias (translatePattern pattern, []) (unprecommented (lowercase (Name.toString (Ann.toValue locatedAlias))))

            Src.PUnit ->
                ElmFormat.Pattern.UnitPattern []

            Src.PTuple fstPattern sndPattern otherPatterns ->
                ElmFormat.Pattern.Tuple (map (uncommented . translatePattern) (fstPattern : sndPattern : otherPatterns))

            Src.PCtor _ constructorName argPatterns ->
                ElmFormat.Pattern.Data [uppercase (Name.toString constructorName)] (map (unprecommented . translatePattern) argPatterns)

            Src.PCtorQual _ qualifier constructorName argPatterns ->
                ElmFormat.Pattern.Data
                    (translateDottedName qualifier
                        ++ [uppercase (Name.toString constructorName)])
                    (map (unprecommented . translatePattern) argPatterns)

            Src.PList elementPatterns ->
                ElmFormat.Pattern.List (map (uncommented . translatePattern) elementPatterns)

            Src.PCons headPattern tailPattern ->
                let
                    absolutelyNoComments a =
                        ([], [], a, Nothing)
                in
                ElmFormat.Pattern.ConsPattern
                    (translatePattern headPattern, Nothing)
                    (map (absolutelyNoComments . translatePattern) (collapseConsPattern tailPattern))

            Src.PChr charAsText ->
                ElmFormat.Pattern.Literal (translateLiteralChar charAsText)

            Src.PStr stringLiteral ->
                ElmFormat.Pattern.Literal (translateLiteralString stringLiteral)

            Src.PInt int ->
                ElmFormat.Pattern.Literal (translateLiteralInt int)


translateLiteralChar :: Text -> ElmFormat.Literal
translateLiteralChar = ElmFormat.Chr . head . Text.unpack


{-|
    The elm compiler saves string literals in javascript string escaping form,
    because the parser handles escaping at parse time.

    This function parses strings that contain encoded values, for example a string
    that would be printed like this:

        \n \t \udbe8

    would be translated to something that is actually "\n \t \[someunicode]".
-}
javascriptStringDecode :: Text -> String
javascriptStringDecode =
    let
        decodeUnicodePieces = \case
            (beforeAnyU:withUAtStart) ->
                Text.unpack beforeAnyU : map decodeUnicodeAtStart withUAtStart

            [] ->
                []

        decodeUnicodeAtStart jsEncodedWithUAtStart =
            let (unicodeCode, restString) = Text.splitAt 4 jsEncodedWithUAtStart in
            unicodeCode
                & Text.unpack
                & Numeric.readHex
                & fst . head
                & pure . Char.chr
                & (++ Text.unpack restString)

    in
    concat
    . decodeUnicodePieces
    . Text.splitOn "\\u"
    . Text.replace "\\\\" "\\"
    . Text.replace "\\'" "'"
    . Text.replace "\\n" "\n"
    . Text.replace "\\r" "\r"
    . Text.replace "\\t" "\t"
    . Text.replace "\\\"" "\""


translateLiteralString :: Text -> ElmFormat.Literal
translateLiteralString stringLiteral =
    ElmFormat.Str (javascriptStringDecode stringLiteral) False


translateLiteralInt :: Int -> ElmFormat.Literal
translateLiteralInt int = ElmFormat.IntNum (fromIntegral int) ElmFormat.DecimalInt


translateLiteralFloat :: Double -> ElmFormat.Literal
translateLiteralFloat double = ElmFormat.FloatNum double ElmFormat.DecimalFloat


collapseConsPattern :: Src.Pattern -> [Src.Pattern]
collapseConsPattern locatedPattern =
    case Ann.toValue locatedPattern of
        Src.PCons headPattern tailPattern ->
            headPattern : collapseConsPattern tailPattern

        _ ->
            [locatedPattern]


translateTypeAnnotation :: Ann.Located Name.Name -> Src.Type -> ElmFormat.Declaration
translateTypeAnnotation locatedName annotatedType =
    let
        qualifierNames =
            (map Text.unpack . Text.split (== '.') . Name.toText . Ann.toValue) locatedName

        qualifier =
            map uppercase (init qualifierNames)

        identifier =
            lowercase (last qualifierNames)
    in
    ElmFormat.TypeAnnotation
        (ElmFormat.VarRef qualifier identifier, [])
        ([], translateType annotatedType)


translateTypeAlias :: Elm.Alias -> ElmFormat.Declaration
translateTypeAlias (Elm.Alias _ locatedName locatedParams aliasType) =
    ElmFormat.TypeAlias []
        (uncommented (translateTypeArgs locatedName locatedParams))
        ([], translateType aliasType)


translateTypeArgs :: Ann.Located Name.Name -> [Ann.Located Name.Name] -> ElmFormat.NameWithArgs ElmFormat.UppercaseIdentifier ElmFormat.LowercaseIdentifier
translateTypeArgs locatedName locatedParams =
    let
        name =
            Name.toString (Ann.toValue locatedName)

        params =
            map (Name.toString . Ann.toValue) locatedParams

        makeTypeParam typeParamName =
            ([], lowercase typeParamName)
    in
    (uppercase name, map makeTypeParam params)


translateUnion :: Elm.Union -> ElmFormat.Declaration
translateUnion (Elm.Union _ locatedName locatedParams constructors) =
    let
        makeType t =
            ([], t)

        makeConstructor (constructorName, types) =
            (uppercase (Name.toString (Ann.toValue constructorName)), map (makeType . translateType) types)
    in
    if null constructors then
        error ("Can't translate Union type with 0 constructors (in union " ++ show (Ann.toValue locatedName) ++ ")")
    else
        ElmFormat.Datatype
            { nameWithArgs = uncommented (translateTypeArgs locatedName locatedParams)
            , tags = openUncommentedList (map makeConstructor constructors)
            }


translateType :: Src.Type -> ElmFormat.Type
translateType locatedType =
    let
        attachNoComments a =
            ([], a)

        typeConstruction name argTypes =
            ElmFormat.TypeConstruction (ElmFormat.NamedConstructor name) (map (attachNoComments . translateType) argTypes)
    in
    unlocated $
        case Ann.toValue locatedType of
            Src.TUnit ->
                ElmFormat.UnitType []

            Src.TType _ constructorName argTypes ->
                typeConstruction [uppercase (Name.toString constructorName)] argTypes

            Src.TVar name ->
                ElmFormat.TypeVariable (lowercase (Name.toString name))

            Src.TLambda argType resultType ->
                let
                    attachAbsolutelyNoComments a =
                        ([], [], a, Nothing)
                in
                ElmFormat.FunctionType
                    { first = (translateType argType, Nothing)
                    , rest = map (attachAbsolutelyNoComments . translateType) (collapseFunctionType resultType)
                    , forceMultiline = ElmFormat.ForceMultiline False
                    }

            Src.TTypeQual _ qualifier constructorName argTypes ->
                typeConstruction
                    (translateDottedName qualifier
                        ++ [uppercase (Name.toString constructorName)])
                    argTypes

            Src.TTuple fstType sndType furtherTypes ->
                let
                    translate =
                        uncommented . unEolCommented . translateType
                in
                ElmFormat.TupleType (translate fstType : translate sndType : map translate furtherTypes)

            Src.TRecord recordFields baseTypeVar ->
                let
                    translateBase =
                        uncommented . lowercase . Name.toString . Ann.toValue

                    translateField (locatedName, fieldType) =
                        translatePair locatedName (translateType fieldType)
                in
                ElmFormat.RecordType
                    { base = fmap translateBase baseTypeVar
                    , fields = uncommentedSequence (map translateField recordFields)
                    , trailingComments = []
                    , forceMultiline = ElmFormat.ForceMultiline False
                    }


translatePair :: Ann.Located Name.Name -> a -> ElmFormat.Pair ElmFormat.LowercaseIdentifier a
translatePair locatedName content =
    ElmFormat.Pair
        (lowercase (Name.toString (Ann.toValue locatedName)), [])
        ([], content)
        (ElmFormat.ForceMultiline False)


translatePort :: Elm.Port -> ElmFormat.Declaration
translatePort (Elm.Port locatedName type_) =
    ElmFormat.PortAnnotation
        (uncommented (lowercase (Name.toString (Ann.toValue locatedName))))
        []
        (translateType type_)


collapseFunctionType :: Src.Type -> [Src.Type]
collapseFunctionType locatedType =
    case Ann.toValue locatedType of
        Src.TLambda arg result ->
            arg : collapseFunctionType result

        _ ->
            [locatedType]


translateEffects :: Elm.Effects -> ElmFormat.SourceTag
translateEffects effects =
    case effects of
        Elm.NoEffects -> ElmFormat.Normal
        Elm.Ports _ -> ElmFormat.Port []
        Elm.Manager _ _ -> ElmFormat.Effect []


translateExposing :: Src.Exposing -> ElmFormat.Listing ElmFormat.DetailedListing
translateExposing =
    \case
        Src.Open ->
            ElmFormat.OpenListing (uncommented ())

        Src.Explicit [] ->
            ElmFormat.ClosedListing

        Src.Explicit exposes ->
            let
                emptyListing =
                    ElmFormat.DetailedListing
                        { ElmFormat.values = Map.empty
                        , ElmFormat.operators = Map.empty
                        , ElmFormat.types = Map.empty
                        }

                insertExpose listings = \case
                    Src.Lower name ->
                        listings
                            { ElmFormat.values =
                                Map.insert
                                    (lowercase (Name.toString name))
                                    (uncommented ())
                                    (ElmFormat.values listings)
                            }

                    Src.Operator name ->
                        listings
                            { ElmFormat.operators =
                                Map.insert
                                    (ElmFormat.SymbolIdentifier (Name.toString name))
                                    (uncommented ())
                                    (ElmFormat.operators listings)
                            }

                    Src.Upper name privacy ->
                        let
                            translatePrivacy = \case
                                Src.Public -> ElmFormat.OpenListing (uncommented ())
                                Src.Private -> ElmFormat.ClosedListing
                        in
                        listings
                            { ElmFormat.types =
                                Map.insert
                                    (uppercase (Name.toString name))
                                    (uncommented ([], translatePrivacy privacy))
                                    (ElmFormat.types listings)
                            }
            in
            ElmFormat.ExplicitListing (foldl insertExpose emptyListing (map Ann.toValue exposes)) False


translateImports :: [Src.Import] -> Map [ElmFormat.UppercaseIdentifier] (ElmFormat.Comments, ElmFormat.ImportMethod)
translateImports = Map.fromList . map translateImport


translateImport :: Src.Import -> ([ElmFormat.UppercaseIdentifier], (ElmFormat.Comments, ElmFormat.ImportMethod))
translateImport (Src.Import locatedName alias exposing) =
    let
        translateAlias aliasName =
            ([], ([], uppercase (Name.toString aliasName)))
    in
    (translateDottedName (Ann.toValue locatedName),
    ([], ElmFormat.ImportMethod (fmap translateAlias alias) ([], ([], translateExposing exposing))))


translateDottedName :: Name.Name -> [ElmFormat.UppercaseIdentifier]
translateDottedName =
    map (uppercase . Text.unpack)
        . Text.split (== '.')
        . Name.toText


-- Utilities


uncommented :: a -> ElmFormat.Commented a
uncommented a = ElmFormat.Commented [] a []


unprecommented :: a -> ElmFormat.PreCommented a
unprecommented a = ([], a)


uncommentedKeyword :: a -> ElmFormat.KeywordCommented a
uncommentedKeyword a = ElmFormat.KeywordCommented [] [] a


unlocated :: a -> ElmFormat.Located a
unlocated a = ElmFormat.at (ElmFormat.Position 0 0) (ElmFormat.Position 0 0) a


unEolCommented :: a -> ElmFormat.WithEol a
unEolCommented a =
    (a, Nothing)


openUncommentedList :: [a] -> ElmFormat.OpenCommentedList a
openUncommentedList ls =
    ElmFormat.OpenCommentedList
        (map (uncommented . unEolCommented) (init ls))
        ([], (last ls, Nothing))

uncommentedSequence :: [a] -> ElmFormat.Sequence a
uncommentedSequence =
    let
        uncomment a = ([], ([], a))
    in
    map (uncomment . unEolCommented)

uppercase :: String -> ElmFormat.UppercaseIdentifier
uppercase =
    ElmFormat.UppercaseIdentifier


lowercase :: String -> ElmFormat.LowercaseIdentifier
lowercase =
    ElmFormat.LowercaseIdentifier



-- Running


renderModule :: ElmFormat.Module -> Text
renderModule = ElmFormat.render ElmFormat.ElmVersion.Elm_0_19


renderType :: ElmFormat.Type -> Text
renderType = ElmFormat.renderBox . ElmFormat.formatType ElmFormat.ElmVersion.Elm_0_19


renderPattern :: ElmFormat.Pattern.Pattern -> Text
renderPattern = ElmFormat.renderBox . ElmFormat.formatPattern ElmFormat.ElmVersion.Elm_0_19 False


renderExpr :: ElmFormat.Expr -> Text
renderExpr =
    ElmFormat.renderBox
    . ElmFormat.formatExpression
        ElmFormat.ElmVersion.Elm_0_19
        (ElmFormat.ImportInfo Map.empty Map.empty)
        ElmFormat.SyntaxSeparated


prettyType :: Src.Type -> String
prettyType = Text.unpack . renderType . translateType


prettyPattern :: Src.Pattern -> String
prettyPattern = Text.unpack . renderPattern . translatePattern


prettyExpr :: Elm.Expr -> String
prettyExpr = Text.unpack . renderExpr . translateExpression
