{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}

-- ment to be imported qualified as Expr
module Language.Elm.Traverse.Expr where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Name as Name
import qualified "elm" Reporting.Annotation as Ann
import qualified AST.Utils.Shader as Shader

-- misc
import Data.Text (Text)
import Control.Lens hiding (List)
import Control.Arrow ((&&&))

import qualified Language.Elm.Lenses as Elm


data F a
    = Chr Text
    | Str Text
    | Int Int
    | Float Double
    | Var Src.VarType Name.Name
    | VarQual Src.VarType Name.Name Name.Name
    | List [a]
    | Op Name.Name
    | Negate a
    | Binops [(a, Ann.Located Name.Name)] a
    | Lambda [Src.Pattern] a
    | Call a [a]
    | If [(a, a)] a
    | Let [DefF a] a
    | Case a [(Src.Pattern, a)]
    | Accessor Name.Name
    | Access a (Ann.Located Name.Name)
    | Update (Ann.Located Name.Name) [(Ann.Located Name.Name, a)]
    | Record [(Ann.Located Name.Name, a)]
    | Unit
    | Tuple a a [a]
    | Shader Text Text Shader.Shader
    deriving (Functor, Foldable, Traversable)

data DefF a
    = Define (Elm.LetDefinition a)
    | Destruct (Elm.LetDestruct a)
    deriving (Functor, Foldable, Traversable)


_DefineF :: Simple Prism (DefF a) (Elm.LetDefinition a)
_DefineF = prism' Define $ \case
    Define define ->
        Just define

    Destruct{} ->
        Nothing


_DestructF :: Simple Prism (DefF a) (Elm.LetDestruct a)
_DestructF = prism' Destruct $ \case
    Destruct destruct ->
        Just destruct

    Define{} ->
        Nothing


patterns :: F a -> [Src.Pattern]
patterns = \case
    Lambda args _ ->
        args

    Let defs _ ->
        concatMap
            (\case
                Define def ->
                    view Elm.letDefArguments def

                Destruct destruct ->
                    [ view Elm.letDestructPattern destruct ]
            )
            defs

    Case _ cases ->
        toListOf (traverse . _1) cases

    _ ->
        []


constructDef :: DefF Elm.Expr -> Elm.Def
constructDef = \case
    Define (Elm.LetDefinition region name arguments body typeAnnotation) ->
        Elm.Define region name arguments body typeAnnotation

    Destruct (Elm.LetDestruct region pattern body) ->
        Elm.Destruct region pattern body


destructDef :: Elm.Def -> DefF Elm.Expr
destructDef = \case
    Elm.Define region name arguments body typeAnnotation ->
        Define (Elm.LetDefinition region name arguments body typeAnnotation)

    Elm.Destruct region pattern body ->
        Destruct (Elm.LetDestruct region pattern body)


makePrisms ''F


asF :: Simple Lens Elm.Expr (Ann.Located (F Elm.Expr))
asF f = fmap construct . f . deconstruct


construct :: Ann.Located (F Elm.Expr) -> Elm.Expr
construct =
    over Elm.valueOfLocated construct_


construct_ :: F Elm.Expr -> Elm.Expr_
construct_ = \case
    Chr char ->
        Elm.Chr char

    Str str ->
        Elm.Str str

    Int int ->
        Elm.Int int

    Float float ->
        Elm.Float float

    Var varType name ->
        Elm.Var varType name

    VarQual varType qualifier name ->
        Elm.VarQual varType qualifier name

    List ofExpr ->
        Elm.List ofExpr

    Op opName ->
        Elm.Op opName

    Negate expr ->
        Elm.Negate expr

    Binops operators lastOperand ->
        Elm.Binops operators lastOperand

    Lambda arguments body ->
        Elm.Lambda arguments body

    Call function arguments ->
        Elm.Call function arguments

    If thens else_ ->
        Elm.If thens else_

    Let letDefinitions in_ ->
        Elm.Let (map constructDef letDefinitions) in_

    Case ofExpr cases ->
        Elm.Case ofExpr cases

    Accessor accessorName ->
        Elm.Accessor accessorName

    Access accessedExpr accessor ->
        Elm.Access accessedExpr accessor

    Update recordName updatedFields ->
        Elm.Update recordName updatedFields

    Record fields ->
        Elm.Record fields

    Unit ->
        Elm.Unit

    Tuple first second more ->
        Elm.Tuple first second more

    Shader vertext fragment shader ->
        Elm.Shader vertext fragment shader


deconstruct :: Elm.Expr -> Ann.Located (F Elm.Expr)
deconstruct =
    over Elm.valueOfLocated deconstruct_


deconstruct_ :: Elm.Expr_ -> F Elm.Expr
deconstruct_ = \case
    Elm.Chr char ->
        Chr char

    Elm.Str str ->
        Str str

    Elm.Int int ->
        Int int

    Elm.Float float ->
        Float float

    Elm.Var varType name ->
        Var varType name

    Elm.VarQual varType qualifier name ->
        VarQual varType qualifier name

    Elm.List ofExpr ->
        List ofExpr

    Elm.Op opName ->
        Op opName

    Elm.Negate expr ->
        Negate expr

    Elm.Binops operators lastOperand ->
        Binops operators lastOperand

    Elm.Lambda arguments body ->
        Lambda arguments body

    Elm.Call function arguments ->
        Call function arguments

    Elm.If thens else_ ->
        If thens else_

    Elm.Let letDefinitions in_ ->
        Let (map destructDef letDefinitions) in_

    Elm.Case ofExpr cases ->
        Case ofExpr cases

    Elm.Accessor accessorName ->
        Accessor accessorName

    Elm.Access accessedExpr accessor ->
        Access accessedExpr accessor

    Elm.Update recordName updatedFields ->
        Update recordName updatedFields

    Elm.Record fields ->
        Record fields

    Elm.Unit ->
        Unit

    Elm.Tuple first second more ->
        Tuple first second more

    Elm.Shader vertext fragment shader ->
        Shader vertext fragment shader

-- A catamorphism
fold :: (Ann.Located (F a) -> a) -> Elm.Expr -> a
fold algebra expr =
    (algebra . (over Elm.valueOfLocated (fmap (fold algebra)))) (deconstruct expr)


-- A paramorphism
foldWithOriginal :: (Ann.Located (F (a, Elm.Expr)) -> a) -> Elm.Expr -> a
foldWithOriginal algebra expr =
    (algebra . (over Elm.valueOfLocated (fmap (foldWithOriginal algebra &&& id)))) (deconstruct expr)


collectWith :: Monoid m => (forall a . F a -> m) -> F m -> m
collectWith baseCase =
    baseCase <> collect


collectDef :: Monoid m => DefF m -> m
collectDef =
    foldMap id


collect :: Monoid m => F m -> m
collect =
    foldMap id
