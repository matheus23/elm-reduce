-- TODO: For all Traverse.* modules: Use the recursion-schemes library, and implement Recursive Src.Type, for example.
-- using this, it is possible to abstract [Type]Declarations.reduceRecursively.
{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}

-- ment to be imported qualified as Type
module Language.Elm.Traverse.Type where

-- Elm compiler
import qualified AST.Source as Src
import qualified Elm.Name as Name
import qualified "elm" Reporting.Annotation as Ann
import qualified "elm" Reporting.Region as Region

-- misc
import Control.Lens hiding (List)
import Control.Arrow ((&&&))

import qualified Language.Elm.Lenses as Elm


data F a
    = Lambda a a
    | Var Name.Name
    | Type Region.Region Name.Name [a]
    | TypeQual Region.Region Name.Name Name.Name [a]
    | Record [(Ann.Located Name.Name, a)] (Maybe (Ann.Located Name.Name))
    | Unit
    | Tuple a a [a]
    deriving (Functor, Foldable, Traversable)


construct :: Ann.Located (F Src.Type) -> Src.Type
construct =
    over Elm.valueOfLocated construct_


construct_ :: F Src.Type -> Src.Type_
construct_ = \case
    Lambda arg result ->
        Src.TLambda arg result

    Var name ->
        Src.TVar name

    Type region name typeApplications ->
        Src.TType region name typeApplications

    TypeQual region qualifier name typeApplications ->
        Src.TTypeQual region qualifier name typeApplications

    Record fields maybeExtension ->
        Src.TRecord fields maybeExtension

    Unit ->
        Src.TUnit

    Tuple first second more ->
        Src.TTuple first second more



deconstruct :: Src.Type -> Ann.Located (F Src.Type)
deconstruct =
    over Elm.valueOfLocated deconstruct_


deconstruct_ :: Src.Type_ -> F Src.Type
deconstruct_ = \case
    Src.TLambda arg result ->
        Lambda arg result

    Src.TVar name ->
        Var name

    Src.TType region name typeApplications ->
        Type region name typeApplications

    Src.TTypeQual region qualifier name typeApplications ->
        TypeQual region qualifier name typeApplications

    Src.TRecord fields maybeExtension ->
        Record fields maybeExtension

    Src.TUnit ->
        Unit

    Src.TTuple first second more ->
        Tuple first second more


fold :: (Ann.Located (F a) -> a) -> Src.Type -> a
fold algebra pattern =
    (algebra . (over Elm.valueOfLocated (fmap (fold algebra)))) (deconstruct pattern)


foldWithOriginal :: (Ann.Located (F (a, Src.Type)) -> a) -> Src.Type -> a
foldWithOriginal algebra pattern =
    (algebra . (over Elm.valueOfLocated (fmap (foldWithOriginal algebra &&& id)))) (deconstruct pattern)


rebuild :: (Ann.Located (F Src.Type) -> Ann.Located (F Src.Type)) -> Src.Type -> Src.Type
rebuild rebuilder =
    fold (construct . rebuilder)


collectWith :: Monoid m => (forall a . F a -> m) -> F m -> m
collectWith baseCase =
    baseCase <> collect


collect :: Monoid m => F m -> m
collect =
    foldMap id
