{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}

-- ment to be imported qualified as Pattern
module Language.Elm.Traverse.Pattern where

-- Elm compiler
import qualified AST.Source as Src
import qualified Elm.Name as Name
import qualified "elm" Reporting.Annotation as Ann
import qualified "elm" Reporting.Region as Region

-- misc
import Data.Text (Text)
import Control.Lens hiding (List)

import qualified Language.Elm.Lenses as Elm


data F a
    = Anything
    | Var Name.Name
    | Record [Ann.Located Name.Name]
    | Alias a (Ann.Located Name.Name)
    | Unit
    | Tuple a a [a]
    | Ctor Region.Region Name.Name [a]
    | CtorQual Region.Region Name.Name Name.Name [a]
    | List [a]
    | Cons a a
    | Chr Text
    | Str Text
    | Int Int
    deriving (Functor, Foldable)


makePrisms ''F


asF :: Simple Lens Src.Pattern (Ann.Located (F Src.Pattern))
asF f = fmap construct . f . deconstruct


construct :: Ann.Located (F Src.Pattern) -> Src.Pattern
construct =
    over Elm.valueOfLocated construct_


construct_ :: F Src.Pattern -> Src.Pattern_
construct_ = \case
    Anything ->
        Src.PAnything

    Var name ->
        Src.PVar name

    Record fields ->
        Src.PRecord fields

    Alias forPattern locatedName ->
        Src.PAlias forPattern locatedName

    Unit ->
        Src.PUnit

    Tuple first second more ->
        Src.PTuple first second more

    Ctor region name argumentPatterns ->
        Src.PCtor region name argumentPatterns

    CtorQual region qualifier name argumentPatterns ->
        Src.PCtorQual region qualifier name argumentPatterns

    List listOfPatterns ->
        Src.PList listOfPatterns

    Cons headPattern tailPattern ->
        Src.PCons headPattern tailPattern

    Chr char ->
        Src.PChr char

    Str str ->
        Src.PStr str

    Int int ->
        Src.PInt int


deconstruct :: Src.Pattern -> Ann.Located (F Src.Pattern)
deconstruct =
    over Elm.valueOfLocated deconstruct_


deconstruct_ :: Src.Pattern_ -> F Src.Pattern
deconstruct_ = \case
    Src.PAnything ->
        Anything

    Src.PVar name ->
        Var name

    Src.PRecord fields ->
        Record fields

    Src.PAlias forPattern locatedName ->
        Alias forPattern locatedName

    Src.PUnit ->
        Unit

    Src.PTuple first second more ->
        Tuple first second more

    Src.PCtor region name argumentPatterns ->
        Ctor region name argumentPatterns

    Src.PCtorQual region qualifier name argumentPatterns ->
        CtorQual region qualifier name argumentPatterns

    Src.PList listOfPatterns ->
        List listOfPatterns

    Src.PCons headPattern tailPattern ->
        Cons headPattern tailPattern

    Src.PChr char ->
        Chr char

    Src.PStr str ->
        Str str

    Src.PInt i ->
        Int i


fold :: (Ann.Located (F a) -> a) -> Src.Pattern -> a
fold algebra pattern =
    (algebra . (over Elm.valueOfLocated (fmap (fold algebra)))) (deconstruct pattern)


rebuild :: (Ann.Located (F Src.Pattern) -> Ann.Located (F Src.Pattern)) -> Src.Pattern -> Src.Pattern
rebuild rebuilder =
    fold (construct . rebuilder)


collectWith :: Monoid m => (forall a . F a -> m) -> F m -> m
collectWith baseCase =
    baseCase <> collect


collect :: Monoid m => F m -> m
collect =
    foldMap id

