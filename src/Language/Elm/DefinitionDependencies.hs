{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Language.Elm.DefinitionDependencies where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Name as Name

-- misc
import qualified Data.Map as Map
import Data.Map (Map)
import qualified Data.Set as Set
import Data.Set (Set)
import qualified Data.Char as Char
import qualified Data.Text as Text
import qualified Data.Maybe as Maybe
import Control.Lens hiding ((<.>), from)

import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.Traverse as Traverse
import qualified Language.Elm.Traverse.Expr as Expr
import qualified Language.Elm.Traverse.Type as Type
import qualified Language.Elm.Traverse.Pattern as Pattern
import Reduce.ImportNormalization.Types
import qualified Reduce.ImportNormalization as ImportNormalization
import Language.Elm.OrphanInstances ()

import qualified Graph
import Graph (Graph)


-- Function Dependencies

referredDeclsAlg :: Traverse.LocallyBound -> Expr.F (Set Name.Name) -> Set Name.Name
referredDeclsAlg locallyBound = Expr.collectWith $ \case
    Expr.Var _ name
        | name `Set.notMember` locallyBound ->
        Set.singleton name

    Expr.Update locatedName _
        | let name = view Elm.valueOfLocated locatedName in
            name `Set.notMember` locallyBound ->
        Set.singleton (view Elm.valueOfLocated locatedName)

    _ ->
        mempty


referredDeclsInExpr :: Elm.Expr -> Set Name.Name
referredDeclsInExpr expr =
    Expr.fold (Traverse.withLocallyBound referredDeclsAlg . view Elm.valueOfLocated) expr Set.empty


declCallTree :: Elm.Module -> Graph Name.Name
declCallTree module_ =
    let
        declarations =
            toListOf (Elm.moduleDeclarations . traverse . Elm.valueOfLocated) module_
        
        ports =
            toListOf (Elm.moduleEffects . Elm._Ports . traverse) module_

        handleDeclaration decl =
            Map.singleton
                (view (Elm.declarationName . Elm.valueOfLocated) decl)
                (Set.toList (referredDeclsInExpr (view Elm.declarationBody decl)))
            
        handlePort port =
            Map.singleton
                (view (Elm.portName . Elm.valueOfLocated) port)
                []
    in
    foldMap handleDeclaration declarations
    <> foldMap handlePort ports


removeDeadDeclarations :: Name.Name -> Elm.Module -> Elm.Module
removeDeadDeclarations mainFunctionName module_ =
    let
        callTree =
            declCallTree module_

        liveDeclarations =
            Graph.reachableFrom mainFunctionName callTree

        isDeclarationAlive decl =
            view (Elm.valueOfLocated . Elm.declarationName . Elm.valueOfLocated) decl
            `elem` liveDeclarations

        isExposingAlive = \case
            Src.Lower name ->
                name `elem` liveDeclarations

            _ ->
                True

        isPortAlive port =
            view (Elm.portName . Elm.valueOfLocated) port
            `elem` liveDeclarations
    in
    module_
        & over Elm.moduleDeclarations
            (filter isDeclarationAlive)
        & over (Elm.moduleExports . Elm.valueOfLocated . Elm._Explicit)
            (filter (isExposingAlive . view Elm.valueOfLocated))
        & over (Elm.moduleEffects . Elm._Ports)
            (filter isPortAlive)


-- Type Dependencies


referredConstructorsAlg :: Traverse.LocallyBound -> Expr.F (Set Name.Name) -> Set Name.Name
referredConstructorsAlg locallyBound expr =
    let
        isConstructorId =
            Text.any Char.isUpper . Text.take 1 . Name.toText

        constructorsReferredInVariables =
            Set.filter isConstructorId (referredDeclsAlg locallyBound expr)

        cosntructorsReferredInPatterns =
            Set.unions (map referredConstructorsInPattern (Expr.patterns expr))
    in
    Set.union constructorsReferredInVariables cosntructorsReferredInPatterns


referredTypesInTypeAlg :: Type.F (Set Name.Name) -> Set Name.Name
referredTypesInTypeAlg = Type.collectWith $ \case
    Type.Type _ name _ | name /= "List" -> -- List special case
        Set.singleton name

    -- Qualified Type references can only refer to imported declarations, we don't record those
    _ ->
        Set.empty


referredConstructorsInPatternAlg :: Pattern.F (Set Name.Name) -> Set Name.Name
referredConstructorsInPatternAlg = Pattern.collectWith $ \case
    Pattern.Ctor _ name _ ->
        Set.singleton name

    _ ->
        Set.empty


referredConstructorsInExpr :: Elm.Expr -> Set Name.Name
referredConstructorsInExpr expr =
    Expr.fold (Traverse.withLocallyBound referredConstructorsAlg . view Elm.valueOfLocated) expr Set.empty


referredTypesInType :: Src.Type -> Set Name.Name
referredTypesInType =
    Type.fold (referredTypesInTypeAlg . view Elm.valueOfLocated)


referredConstructorsInPattern :: Src.Pattern -> Set Name.Name
referredConstructorsInPattern =
    Pattern.fold (referredConstructorsInPatternAlg . view Elm.valueOfLocated)


referredConstructorsInDeclaration :: Elm.Decl -> Set Name.Name
referredConstructorsInDeclaration decl =
    let
        inExpr =
            referredConstructorsInExpr (view Elm.declarationBody decl)

        arguments =
            toListOf (Elm.declarationArguments . traverse) decl

        inArguments =
            Set.unions (map referredConstructorsInPattern arguments)
    in
    inArguments <> inExpr


referredTypesInDeclaration :: Map Name.Name Name.Name -> Elm.Decl -> Set Name.Name
referredTypesInDeclaration constructorLookup decl =
    Maybe.maybe
        Set.empty -- Nothing case
        referredTypesInType -- Just case
        (view Elm.declarationTypeAnnotation decl) -- of
    <> Set.map (constructorLookup Map.!) (referredConstructorsInDeclaration decl)


typeDependencyGraph :: Elm.Module -> Graph Name.Name
typeDependencyGraph module_ =
    let
        moduleTypeIdentifierDeclarations =
            ImportNormalization.moduleDefinedAliasIdentifiers module_
            <> ImportNormalization.moduleDefinedUnionIdentifiers module_

        createConstructorLookup = \case
            AliasConstructor name ->
                Map.singleton name name

            UnionConstructor unionName constructorName ->
                Map.singleton constructorName unionName

            _ ->
                Map.empty -- TODO: Maybe produce errors? Or refactor?

        constructorLookup =
            Map.unions (map createConstructorLookup moduleTypeIdentifierDeclarations)

        declarations =
            toListOf (Elm.moduleDeclarations . traverse . Elm.valueOfLocated) module_

        handleDeclaration decl =
            Map.singleton
                (view (Elm.declarationName . Elm.valueOfLocated) decl)
                (Set.toList (referredTypesInDeclaration constructorLookup decl))

        handleAlias alias =
            Map.singleton
                (view (Elm.aliasName . Elm.valueOfLocated) alias)
                (Set.toList (referredTypesInType (view Elm.aliasType alias)))

        handleUnion union =
            let
                typesInUnion =
                    toListOf (Elm.unionConstructors . traverse . _2 . traverse) union
            in
            Map.singleton
                (view (Elm.unionName . Elm.valueOfLocated) union)
                (Set.toList (Set.unions (map referredTypesInType typesInUnion)))
    in
    Map.unions
        [ foldMap handleDeclaration declarations
        , foldMap handleAlias (view Elm.moduleAliases module_)
        , foldMap handleUnion (view Elm.moduleUnions module_)
        ]

-- constructor references

constructorReferences :: Elm.Module -> Graph Name.Name
constructorReferences =
    let
        handleDeclaration decl =
            Map.singleton
                (view (Elm.declarationName . Elm.valueOfLocated) decl)
                (Set.toList (referredConstructorsInDeclaration decl))
    in
    foldMap handleDeclaration
    . toListOf (Elm.moduleDeclarations . traverse . Elm.valueOfLocated)
