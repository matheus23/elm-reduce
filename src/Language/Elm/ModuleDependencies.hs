{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}

module Language.Elm.ModuleDependencies where

-- Elm compiler
import qualified File.Find as Find
import qualified AST.Valid as Elm
import qualified Elm.Package as Pkg
import qualified Elm.Compiler.Module as Module
import qualified Elm.Project.Json as Json
import qualified Elm.Project.Summary as Summary
import qualified Elm.PerUserCache as ElmPaths

-- misc
import qualified Data.Map as Map
-- import Data.Map (Map)
import Data.Maybe as Maybe
import Data.Foldable (traverse_)
import System.FilePath
import Control.Lens hiding ((<.>), from)
import Control.Monad.Trans.State
import Control.Monad.Trans.Class (lift)
import Control.Monad (unless)

import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import Language.Elm.OrphanInstances ()
import qualified Graph
import Graph (Graph)


data ElmSource
    = Local FilePath
    | Foreign Pkg.Package FilePath
    deriving (Show, Eq, Ord)


parseProjectModule :: Summary.Summary -> Module.Raw -> IO Elm.Module
parseProjectModule summary moduleName = do
    pathFromModuleName summary moduleName >>= \case
        Local path ->
            Elm.parseModuleFromFile (Json.getName (Summary._project summary)) path

        Foreign pkg path ->
            Elm.parseModuleFromFile (Pkg._name pkg) path


pathFromModuleName :: Summary.Summary -> Module.Raw -> IO ElmSource
pathFromModuleName (Summary.Summary root project exposed _ _) moduleName = do
    -- logic adapted form 'findElm' in File.Find in elm-compiler
    let srcDirs =
            case project of
                Json.App app ->
                    map (root </>) (Json._app_source_dirs app)

                Json.Pkg _ ->
                    [ root </> "src" ]

    paths <- Maybe.catMaybes <$> mapM (Find.elmExists moduleName) srcDirs

    case (paths, Map.lookup moduleName exposed) of
        ([path], Nothing) ->
            return (Local path)

        ([], Just [Pkg.Package name version]) -> do
            packageRoot <- ElmPaths.getPackageRoot
            let packageDir = Pkg.toFilePath name </> Pkg.versionToString version
            let modulePath = "src" </> Module.nameToSlashPath moduleName <.> "elm"
            -- usually something like ~/.elm/0.19/packages/elm/core/1.0.2/src/Basics.elm
            return
                (Foreign
                    (Pkg.Package name version)
                    (packageRoot </> packageDir </> modulePath))

        ([], Nothing) -> do
            print root
            -- TODO: Something better than error
            error ("Module Not Found: " ++ show moduleName)

        (_, maybePkgs) ->
            error ("Module ambiguous: " ++ show moduleName ++ ", " ++ show paths ++ ", more info: " ++ show maybePkgs)


importedModules :: Elm.Module -> [Module.Raw]
importedModules =
    toListOf (Elm.moduleImports . traverse . Elm.importName . Elm.valueOfLocated)


data Dependency
    = Dependency
    { depModuleName :: Module.Raw
    , depSource :: ElmSource
    }
    deriving (Eq, Show, Ord)


localDependencies :: Module.Raw -> Summary.Summary -> IO (Graph FilePath)
localDependencies mainModule summary =
    let
        addDependencies :: FilePath -> StateT (Graph FilePath) IO ()
        addDependencies path = do
            graph <- get
            unless (Map.member path graph) $ do
                deps <- lift $ filterLocalDependencies <$> dependenciesFromSingleModule summary path
                modify (Map.insert path deps)
                traverse_ addDependencies deps
    in do
    Local mainModulePath <- pathFromModuleName summary mainModule
    execStateT (addDependencies mainModulePath) Map.empty


filterLocalDependencies :: [Dependency] -> [FilePath]
filterLocalDependencies =
    let
        onlyLocalDependency (Dependency _ source) =
            case source of
                Local path ->
                    Just path

                Foreign{} ->
                    Nothing
    in
    mapMaybe onlyLocalDependency


dependenciesFromSingleModule :: Summary.Summary -> FilePath -> IO [Dependency]
dependenciesFromSingleModule summary modulePath =
    let
        makeDependencyFromModule dependentModule =
            Dependency dependentModule <$> pathFromModuleName summary dependentModule
    in do
        module_ <- Elm.parseModuleFromFile (Elm.summaryPkgName summary) modulePath
        traverse makeDependencyFromModule (importedModules module_)


-- TODO: Let it also figure out the module names so that the output is cleaner
localDependenciesDotfile :: FilePath -> Module.Raw -> IO String
localDependenciesDotfile rootPath mainModule =
    Elm.parseProject rootPath
        >>= localDependencies mainModule
        <&> Graph.dotfile
