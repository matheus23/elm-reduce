{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# OPTIONS_GHC -Wall #-}

module Language.Elm.Rename where

-- Elm compiler

import qualified AST.Source as Src
import qualified AST.Valid as Elm
-- misc

import Control.Lens hiding ((<.>))
import qualified Data.Set as Set
import Data.Set (Set)
import qualified Elm.Name as Name
import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.Traverse as Traverse
import qualified Language.Elm.Traverse.Expr as Expr
import qualified Language.Elm.Traverse.Pattern as Pattern
import qualified Language.Elm.Traverse.Type as Type
import Reduce.ImportNormalization.Types
import qualified "elm" Reporting.Annotation as Ann

type LocallyBound =
  Set Name.Name

type QualId = (Maybe Name.Name, Name.Name)

data IdentifierUsage
  = -- Value Level
    BindDeclaration Name.Name
  | BindLocal Name.Name
  | UsageFunction (Maybe Name.Name) Name.Name
  | -- Type Level
    BindType Name.Name
  | UsageType (Maybe Name.Name) Name.Name

usageToId :: IdentifierUsage -> Identifier
usageToId = \case
  BindDeclaration name ->
    Id FunctionNamespace Nothing name
  BindLocal name ->
    Id FunctionNamespace Nothing name
  UsageFunction qualifier name ->
    Id FunctionNamespace qualifier name
  BindType name ->
    Id TypeNamespace Nothing name
  UsageType Nothing "List" ->
    ListId
  UsageType qualifier name ->
    Id TypeNamespace qualifier name

renameNonshadowed :: (Identifier -> QualId) -> LocallyBound -> IdentifierUsage -> QualId
renameNonshadowed renameFlat boundIdentifiers renamingBinder =
  case usageToId renamingBinder of
    Id FunctionNamespace Nothing name
      | name `Set.member` boundIdentifiers ->
        (Nothing, name)
    otherIdentifiers ->
      renameFlat otherIdentifiers

inModule :: (LocallyBound -> IdentifierUsage -> QualId) -> Elm.Module -> Elm.Module
inModule rename module_ =
  let renameFlat =
        rename Set.empty
   in module_
        & over (Elm.moduleExports . Elm.valueOfLocated) (inExports renameFlat)
        & over (Elm.moduleAliases . mapped) (inAlias renameFlat)
        & over (Elm.moduleUnions . mapped) (inUnion renameFlat)
        & over (Elm.moduleEffects . Elm._Ports . mapped) (inPort renameFlat)
        & over (Elm.moduleDeclarations . mapped . Elm.valueOfLocated) (inDeclaration rename)

inExports :: (IdentifierUsage -> QualId) -> Src.Exposing -> Src.Exposing
inExports rename =
  let renameExposed = \case
        Src.Lower name ->
          Src.Lower (snd . rename . BindDeclaration $ name)
        Src.Upper name privacy ->
          Src.Upper (snd . rename . BindType $ name) privacy
        Src.Operator name ->
          Src.Operator name
   in over (Elm._Explicit . mapped . Elm.valueOfLocated) renameExposed

inAlias :: (IdentifierUsage -> QualId) -> Elm.Alias -> Elm.Alias
inAlias rename =
  over (Elm.aliasName . Elm.valueOfLocated) (snd . rename . BindType)
    . over Elm.aliasType (inType rename)

inUnion :: (IdentifierUsage -> QualId) -> Elm.Union -> Elm.Union
inUnion rename =
  over (Elm.unionName . Elm.valueOfLocated) (snd . rename . BindType)
    . over (Elm.unionConstructors . mapped . _1 . Elm.valueOfLocated) (snd . rename . BindDeclaration)
    . over (Elm.unionConstructors . mapped . _2 . mapped) (inType rename)

inPort :: (IdentifierUsage -> QualId) -> Elm.Port -> Elm.Port
inPort rename =
  over (Elm.portName . Elm.valueOfLocated) (snd . rename . BindDeclaration)
    . over Elm.portType (inType rename)

inDeclaration :: (LocallyBound -> IdentifierUsage -> QualId) -> Elm.Decl -> Elm.Decl
inDeclaration rename decl =
  decl
    & over (Elm.declarationTypeAnnotation . _Just) (inType (rename Set.empty))
    & over Elm.declarationBody (inExpr rename (Traverse.boundByPatterns (view Elm.declarationArguments decl)))
    & over (Elm.declarationArguments . mapped) (inPattern (rename Set.empty))
    & over (Elm.declarationName . Elm.valueOfLocated) (snd . rename Set.empty . BindDeclaration)

inType :: (IdentifierUsage -> QualId) -> Src.Type -> Src.Type
inType =
  Type.rebuild . over Elm.valueOfLocated . renameTypeAlg

renameTypeAlg :: (IdentifierUsage -> QualId) -> Type.F a -> Type.F a
renameTypeAlg rename =
  let renamedTypeId region = \case
        (Just renamedQual, renamed) ->
          Type.TypeQual region renamedQual renamed
        (Nothing, renamed) ->
          Type.Type region renamed
   in \case
        Type.Type region name parameterApplications ->
          renamedTypeId
            region
            (rename (UsageType Nothing name))
            parameterApplications
        Type.TypeQual region qualName name parameterApplications ->
          renamedTypeId
            region
            (rename (UsageType (Just qualName) name))
            parameterApplications
        other ->
          other

inExpr :: (LocallyBound -> IdentifierUsage -> QualId) -> LocallyBound -> Elm.Expr -> Elm.Expr
inExpr rename locallyBound expr =
  let exprBuilder :: Ann.Located (Expr.F (LocallyBound -> Elm.Expr)) -> LocallyBound -> Elm.Expr
      exprBuilder locatedExpr bound =
        Expr.construct
          -- a little bit of reordering functors :/
          ( fmap
              ($ bound)
              ( over
                  Elm.valueOfLocated
                  (Traverse.withLocallyBound (renameExprAlg rename))
                  locatedExpr
              )
          )
   in Expr.fold exprBuilder expr locallyBound

renameExprAlg :: (LocallyBound -> IdentifierUsage -> QualId) -> LocallyBound -> Expr.F Elm.Expr -> Expr.F Elm.Expr
renameExprAlg rename locallyBound =
  let renamedVariable varType = \case
        (Just renamedQual, renamed) ->
          Expr.VarQual varType renamedQual renamed
        (Nothing, renamed) ->
          Expr.Var varType renamed
   in \case
        Expr.Var varType name ->
          renamedVariable varType (rename locallyBound (UsageFunction Nothing name))
        Expr.VarQual varType qualifier name ->
          renamedVariable varType (rename locallyBound (UsageFunction (Just qualifier) name))
        Expr.Update recordName updatedFields ->
          case rename locallyBound (UsageFunction Nothing (view Elm.valueOfLocated recordName)) of
            (Nothing, renamedName) ->
              Expr.Update (set Elm.valueOfLocated renamedName recordName) updatedFields
            (Just qualifier, renamedName) ->
              let placeholderName =
                    Elm.dummyLocation "insertedNameForQualifiedRecordUpdate"
               in Expr.Let
                    [ Expr.Define $ Elm.LetDefinition
                        { _letDefRegion = Elm.dummyRegion,
                          _letDefName = placeholderName,
                          _letDefArguments = [],
                          _letDefBody = (Expr.construct . Elm.dummyLocation) (Expr.VarQual Src.Value qualifier renamedName),
                          _letDefTypeAnnotation = Nothing
                        }
                    ]
                    ((Expr.construct . Elm.dummyLocation) (Expr.Update placeholderName updatedFields))
        -- Type renames:

        Expr.Let defs in_ ->
          Expr.Let (map (renameLetAlg rename locallyBound) defs) in_
        -- Pattern renames:

        Expr.Lambda arguments body ->
          Expr.Lambda (map (inPattern (rename locallyBound)) arguments) body
        Expr.Case ofExpr cases ->
          Expr.Case ofExpr (over (mapped . _1) (inPattern (rename locallyBound)) cases)
        -- Trivial cases:

        other ->
          other

renameLetAlg :: (LocallyBound -> IdentifierUsage -> QualId) -> LocallyBound -> Expr.DefF a -> Expr.DefF a
renameLetAlg rename locallyBound =
  over
    Expr._DefineF
    ( over (Elm.letDefArguments . traverse) (inPattern (rename locallyBound))
        . over (Elm.letDefTypeAnnotation . _Just) (inType (rename locallyBound))
        . over (Elm.letDefName . Elm.valueOfLocated) (snd . rename locallyBound . BindLocal)
    )
    . over
      Expr._DestructF
      ( over Elm.letDestructPattern (inPattern (rename locallyBound))
      )

inPattern :: (IdentifierUsage -> QualId) -> Src.Pattern -> Src.Pattern
inPattern =
  Pattern.rebuild . over Elm.valueOfLocated . renamePatternAlg

renamePatternAlg :: (IdentifierUsage -> QualId) -> Pattern.F a -> Pattern.F a
renamePatternAlg rename =
  let renamedConstructor region = \case
        (Just renamedQual, renamed) ->
          Pattern.CtorQual region renamedQual renamed
        (Nothing, renamed) ->
          Pattern.Ctor region renamed
   in \case
        Pattern.Ctor region name arguments ->
          renamedConstructor
            region
            (rename (UsageFunction Nothing name))
            arguments
        Pattern.CtorQual region qualifier name arguments ->
          renamedConstructor
            region
            (rename (UsageFunction (Just qualifier) name))
            arguments
        Pattern.Var name ->
          Pattern.Var (snd (rename (BindLocal name)))
        other ->
          other
