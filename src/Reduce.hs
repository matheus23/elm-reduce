{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TypeApplications #-}

module Reduce where

import qualified Data.Map as Map
-- import Data.Map (Map)
import Control.Monad (void)
import Control.Monad.Extra (findM)
import qualified System.Process as Process
import System.Exit
import qualified Data.IORef as IORef

import qualified Graph
import Graph (Graph)

data Interestingness = Interesting | Uninteresting

data Variant state delta = Variant
    { state :: state
    , delta :: delta
    }
    deriving (Show, Eq, Read, Functor)


data Description = Description
    { shortDescription :: String
    , longDescription :: String
    }
    deriving (Show, Eq)


data Reduction state delta = Reduction
    { phaseName :: String
    , variantsOf :: state -> [Variant state delta]
    , applyVariant :: Variant state delta -> IO ()
    , variantDescription :: Variant state delta -> Description
    }


type SimpleVariant a = Variant a a


mapSimple :: (a -> b) -> SimpleVariant a -> SimpleVariant b
mapSimple f = mapState f . mapDelta f


mapState :: (a -> b) -> Variant a delta -> Variant b delta
mapState f variant = variant { state = f (state variant) }


mapDelta :: (a -> b) -> Variant state a -> Variant state b
mapDelta f variant = variant { delta = f (delta variant) }


cmd :: FilePath -> String -> [String] -> IO String
cmd testDir command args = do
    (exitCode, out, err) <- Process.readCreateProcessWithExitCode (Process.proc command args) { Process.cwd = Just testDir } ""
    case exitCode of
        ExitFailure codeNum -> do
            putStrLn out
            putStrLn ("Error during " ++ command ++ " " ++ show args ++ " (" ++ show codeNum ++ "): " ++ err)
            return out

        ExitSuccess ->
            return out


git :: FilePath -> [String] -> IO String
git testDir args = cmd testDir "git" args


descriptionCommitMessage :: Description -> String
descriptionCommitMessage description =
    shortDescription description <> "\n\n" <> longDescription description


greedily :: Reduction state delta -> IO Interestingness -> IO String -> FilePath -> state -> IO state
greedily Reduction{ phaseName, variantsOf, applyVariant, variantDescription } testCase reductionReport projectRoot initialState = do
    failedItersRef <- IORef.newIORef @Integer 0
    let
        isInterestingTestCase variant = do
            applyVariant variant
            putStrLn ("Variant: " <> shortDescription (variantDescription variant))
            testCase >>= \case
                Interesting -> do
                    output <- git projectRoot ["status", "-s"]
                    if null output then do
                        putStrLn "No changes applied by variant"
                        IORef.modifyIORef failedItersRef (+1)
                        return False
                    else do
                        failedIterations <- IORef.readIORef failedItersRef
                        IORef.writeIORef failedItersRef 0
                        void $ git projectRoot ["add", "-A"]
                        void $ git projectRoot
                            [ "commit"
                            , "-m"
                            , "[elm-reduce " <> show failedIterations <> "] "
                                <> phaseName <> ": "
                                <> descriptionCommitMessage (variantDescription variant)
                            ]
                        return True -- in the sense of "found it!"

                Uninteresting -> do
                    void $ git projectRoot ["reset", "--hard", "HEAD"]
                    IORef.modifyIORef failedItersRef (+1)
                    return False

        loop variantState =
            findM isInterestingTestCase (variantsOf variantState) >>= \case
                Just interestingVariant -> do
                    report <- reductionReport
                    putStrLn ("Reduced: " <> report)
                    loop (state interestingVariant)

                Nothing ->
                    return variantState

    loop initialState


deadReferencesVariants :: Ord a => (a -> Bool) -> Graph a -> [Variant (Graph a) a]
deadReferencesVariants isException graph =
    let
        makeVariant removedNode =
            Variant
                { state = Map.delete removedNode graph
                , delta = removedNode
                }
    in
    (map makeVariant . filter (not . isException) . Graph.neverReferenced) graph


removeElementVariants :: [a] -> [Variant [a] a]
removeElementVariants = \case
    [] ->
        []

    (x:xs) ->
        Variant
            { state = xs
            , delta = x
            }
        : map (mapState ((:) x)) (removeElementVariants xs)
