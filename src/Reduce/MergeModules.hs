{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TupleSections #-}

module Reduce.MergeModules where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Name as Name
import qualified Elm.Compiler.Module as Module
import qualified "elm" Reporting.Annotation as Ann
import qualified Elm.Project.Summary as Summary

-- misc
import qualified Data.Char as Char
import qualified Data.List as List
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Data.Set as Set
import qualified Data.Map as Map
-- import Data.Map (Map)
import Control.Lens hiding ((<.>))
import Data.Function (on)
import Control.Monad
import qualified System.Directory as Dir
import System.FilePath

import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.AST as AST
import qualified Language.Elm.Rename as Rename
import qualified Language.Elm.ModuleDependencies as ModuleDependencies
import Language.Elm.OrphanInstances ()
import qualified Reduce
import qualified Reduce.ImportNormalization as ImportNormalization
import Reduce.ImportNormalization.Types

import qualified Graph
import Graph (Graph)

{- Note

    # Whats renamed?
    Every identifier from the foreign module. That means:
        - identifiers in the parenthesis in `module X exposing (abc, MyUnion(..), EtCetera)`
        - Union type names
        - Union type constructors
        - Type alias names
        - declaration names
        - port names

    # How is everything renamed?
    For example, `view` from the module `Page.Blank` gets renamed to `fromModule_Page_Blank__view`.
    Or `Model` from the module `Page.Article` gets renamed to `FromModule_Page_Article_Model`.
-}

-- | Assumes that both modules are in import-normlized form!
mergeWith :: Elm.Module -> Elm.Module -> Elm.Module
mergeWith foreignModule module_ =
    let
        foreignModuleName =
            Elm._name foreignModule

        -- Prepare Foreign Module

        mainModuleIdentifiers =
            ImportNormalization.moduleDefinedIdentifiers module_
                & map (declarationToIdentifier Nothing)
                & Set.fromList

        isShadowedByTopLevel name =
            Id FunctionNamespace Nothing name `Set.member` mainModuleIdentifiers

        preparedForeignModule =
            foreignModule
                -- Rename all top-level identifiers
                & Rename.inModule
                    (\locallyBound -> \case
                        Rename.BindLocal name | isShadowedByTopLevel name ->
                            (Nothing, Name.fromText (Name.toText name <> "_local"))

                        Rename.UsageFunction Nothing name | name `Set.member` locallyBound && isShadowedByTopLevel name ->
                            (Nothing, Name.fromText (Name.toText name <> "_local"))

                        anyOther ->
                            Rename.renameNonshadowed renameTopLevel locallyBound anyOther
                    )

        foreignTopLevelIdentifiers =
            ImportNormalization.moduleDefinedIdentifiers foreignModule
                & map (declarationToIdentifier Nothing)
                & Set.fromList

        renameTopLevel = \case
            identifier@(Id _ qualifier name) ->
                if identifier `Set.member` foreignTopLevelIdentifiers then
                    -- foreign Ids:
                    (Nothing, ensureRenamed foreignModuleName name)
                else
                    -- non-foreign Ids:
                    (qualifier, name)

            Op name ->
                (Nothing, name)

            ListId ->
                (Nothing, "List")

        -- Prepare host module

        preparedModule =
            module_
                -- Remove the foreign import
                & over Elm.moduleImports (filter ((/=) foreignModuleName . view (Elm.importName . Elm.valueOfLocated)))
                -- Rename references to the foreign definitions
                & Rename.inModule (Rename.renameNonshadowed removeForeignQualifiersAndRename)

        removeForeignQualifiersAndRename = \case
            Id _ qualifier name ->
                if qualifier == Just foreignModuleName then
                    (Nothing, ensureRenamed foreignModuleName name)
                else
                    (qualifier, name)

            Op name ->
                (Nothing, name)

            ListId ->
                (Nothing, "List")
    in
    basicMergeWith preparedForeignModule preparedModule

-- | Simply merges two modules. No renaming of any of the identifiers, just basic copying of definitions from one to the other.
-- Assumes, that both modules are in import normalized form.
basicMergeWith :: Elm.Module -> Elm.Module -> Elm.Module
basicMergeWith foreignModule module_ =
    Elm.Module
        { _name     = Elm._name module_
        , _overview = Elm._overview module_
        , _docs     = Elm._docs module_ -- we don't need docs merging.
        , _exports  = Elm.dummyLocation ((mergeExportsWith `on` view Elm.valueOfLocated . Elm._exports) foreignModule module_)
        , _imports  = (mergeImportsWith `on` Elm._imports) foreignModule module_
        , _decls    = (mappend `on` Elm._decls) foreignModule module_
        , _unions   = (mappend `on` Elm._unions) foreignModule module_
        , _aliases  = (mappend `on` Elm._aliases) foreignModule module_
        , _binop    = (mappend `on` Elm._binop) foreignModule module_
        , _effects  = (mergeEffectsWith `on` Elm._effects) foreignModule module_
        }


renameIdentifier :: Module.Raw -> Name.Name -> Name.Name
renameIdentifier moduleName name =
    let
        moduleNameText =
            Name.toText moduleName

        identifierText =
            Name.toText name

        moduleNameUnderscores =
            Text.replace "." "_" moduleNameText

        prefix =
            -- Check for uppercase, not for namespace, since constructors live in
            -- the function namespace, but are written uppercase!
            if Name.toText name & Text.head & Char.isUpper then
                "FromModule_"
            else
                "fromModule_"
    in
    Name.fromText (prefix <> moduleNameUnderscores <> "__" <> identifierText)


ensureRenamed :: Module.Raw -> Name.Name -> Name.Name
ensureRenamed moduleName possiblyRenamed =
    if Name.startsWith "fromModule_" possiblyRenamed
        || Name.startsWith "FromModule_" possiblyRenamed then
        possiblyRenamed
    else
        renameIdentifier moduleName possiblyRenamed


mergeExportsWith :: Src.Exposing -> Src.Exposing -> Src.Exposing
mergeExportsWith foreignExposing exposing =
    case (foreignExposing, exposing) of
        (Src.Open, exposing') ->
            exposing'

        (exposing', Src.Open) ->
            exposing'

        (Src.Explicit foreignExposed, Src.Explicit exposed) ->
            Src.Explicit (foreignExposed <> exposed)


-- | This function assumes normalized imports
mergeImportsWith :: [Src.Import] -> [Src.Import] -> [Src.Import]
mergeImportsWith foreignImports imports =
    let
        sameImportModule (Src.Import locatedName _ _) (Src.Import otherLocatedName _ _) =
            Ann.toValue locatedName == Ann.toValue otherLocatedName
    in
    -- We use unionBy so we preserve operator imports.
    -- Since both imports should be in normalized form,
    -- equal module imports should both have the same (all) binops imported
    List.unionBy sameImportModule foreignImports imports


mergeEffectsWith :: Elm.Effects -> Elm.Effects -> Elm.Effects
mergeEffectsWith foreignEffect effect =
    case (foreignEffect, effect) of
        (Elm.NoEffects, effect') ->
            effect'

        (effect', Elm.NoEffects) ->
            effect'

        -- TODO: Nice error handling
        (Elm.Manager{}, _) ->
            error "Can't merge manager modules"

        (_, Elm.Manager{}) ->
            error "Can't merge manager modules"

        (Elm.Ports foreignPorts, Elm.Ports ports) ->
            Elm.Ports (ports <> foreignPorts)


mergeModuleVariants :: Graph FilePath -> [Reduce.Variant (Graph FilePath) (FilePath, FilePath)]
mergeModuleVariants depGraph =
    let
        makeVariant (onlyImporter, imported) =
            Reduce.Variant
                { delta = (onlyImporter, imported)
                , state =
                    let
                        inheritedDeps =
                            depGraph Map.! imported
                    in
                    depGraph
                        & Map.delete imported
                        & Map.adjust
                            (filter (/= imported) . List.nub . (++ inheritedDeps))
                            onlyImporter
                }
    in
    map makeVariant (Graph.referencedOnce depGraph)


writeModuleDepGraphfile :: FilePath -> Graph Module.Raw -> IO ()
writeModuleDepGraphfile projectRoot depGraph =
    let
        depFile =
            "ModuleDependencies"
    in do
    writeFile (projectRoot </> depFile <.> "dot") (Graph.dotfile depGraph)
    void $ Reduce.cmd projectRoot "dot" ["-Tpng", "-o", depFile <.> "png", depFile <.> "dot"]


reduceModuleDeps :: String -> Module.Raw -> FilePath -> IO Reduce.Interestingness -> IO String -> IO (Graph FilePath)
reduceModuleDeps phaseName mainModule projectRoot reductionReport testCase = do
    summary <- Elm.parseProject projectRoot

    let
        description (referrer, dependency) =
            Reduce.Description
                { shortDescription = "Merging module " ++ show dependency ++ " into " ++ show referrer
                , longDescription = ""
                }

        processModuleName =
            Elm.parseModuleFromFile (Elm.summaryPkgName summary)
            >=> ImportNormalization.normalizeImports summary

        applyMerge (Reduce.Variant _ (referrerPath, dependencyPath)) = do
            -- writeModuleDepGraphfile projectRoot state

            referrer   <- processModuleName referrerPath
            dependency <- processModuleName dependencyPath

            referrer
                & mergeWith dependency
                & AST.translateModule
                & AST.renderModule
                & Text.writeFile referrerPath

            Dir.removeFile dependencyPath

    depGraph <- ModuleDependencies.localDependencies mainModule summary
    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = mergeModuleVariants
            , applyVariant = applyMerge
            , variantDescription = description . Reduce.delta
            }
        reductionReport testCase projectRoot depGraph


deadModuleVariants :: FilePath -> Graph FilePath -> [Reduce.Variant (Graph FilePath) FilePath]
deadModuleVariants mainModule =
    Reduce.deadReferencesVariants (== mainModule)


reduceDeadModules :: String -> Module.Raw -> FilePath -> IO Reduce.Interestingness -> IO String -> IO (Graph FilePath)
reduceDeadModules phaseName mainModule projectRoot testCase reductionReport = do
    summary <- Elm.parseProject projectRoot

    -- We want to parse as little .elm files as possible, as many elm files are
    -- example modules often containing bugs (even syntax errors), as they're never compiled.
    -- We only parse modules that are reachable from the main module like this and delete the
    -- example modules carfully by still adding stale nodes to them using getElmFiles.
    allElmFiles <- Elm.getElmFiles projectRoot (Summary._project summary)
    depGraph <-
        Map.unionWith (<>) (Map.fromList (zip allElmFiles (repeat []))) -- add all elm files in the project as possibly appearing in the dep graph
        <$> ModuleDependencies.localDependencies mainModule summary -- only build the graph from the main module outwards

    -- TODO: Change CLI so we don't use --main-module Main anymore, but something like src/Main.elm (actual file paths)
    ModuleDependencies.Local mainModulePath <- ModuleDependencies.pathFromModuleName summary mainModule

    let
        deleteModule (Reduce.Variant _ path) = do
            -- TODO: reintroduce? It's great for debugging...
            -- writeModuleDepGraphfile projectRoot state
            Dir.removeFile path

        description (Reduce.Variant _ removedModulePath) =
            Reduce.Description
                { shortDescription = "Removed unreferenced module: " ++ show removedModulePath
                , longDescription = ""
                }

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = deadModuleVariants mainModulePath
            , applyVariant = deleteModule
            , variantDescription = description
            }
        testCase
        reductionReport
        projectRoot
        depGraph
