{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

module Reduce.Definitions where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Compiler.Module as Module
import qualified Elm.Package as Pkg
import qualified Elm.Name as Name

-- misc
import Control.Lens hiding ((<.>), from)
import qualified Data.Text.IO as Text

import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import Language.Elm.OrphanInstances ()
import qualified Language.Elm.ModuleDependencies as ModuleDependencies
import qualified Language.Elm.DefinitionDependencies as DefinitionDependencies
import qualified Language.Elm.AST as AST
import qualified Language.Elm.Traverse.Pattern as Pattern
import qualified Language.Elm.Traverse.Expr as Expr

import qualified Reduce
import qualified Reduce.DeltaDebugging as DD
import qualified Reduce.Reductions as Reduce
import Reduce.Reductions (Reductions(..))

import Graph (Graph)


-- Reduction by utilizing call graph:


-- Note: Not sorting the graph topologically and then removing elements
-- This endavour turned out to
-- 1. Not be faster than calculating the 'neverReferenced' names in the graph every time
-- 2. Not work well, once a function couldn't be removed, because it needed to be preserved for the interestingness
-- (it fails to delete about 3/4 declarations successfully then)
reduceDead :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO (Graph Name.Name)
reduceDead phaseName projectDir testCase reductionReport modulePath = do
    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        callTree =
            DefinitionDependencies.declCallTree module_

        description deletedFunction =
            Reduce.Description
                { shortDescription = "Deleted declaration " ++ show deletedFunction
                , longDescription = ""
                }

        removeDecl nameToBeDeleted =
            removeDeclarationInFile nameToBeDeleted modulePath

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = Reduce.deadReferencesVariants (const False)
            , applyVariant = removeDecl . Reduce.delta
            , variantDescription = description . Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        callTree


removeDeclarationInFile :: Name.Name -> FilePath -> IO ()
removeDeclarationInFile name path =
    Elm.parseModuleFromFile Pkg.dummyName path
    <&> removeDeclaration name
    <&> AST.translateModule
    <&> AST.renderModule
    >>= Text.writeFile path


removeDeclaration :: Name.Name -> Elm.Module -> Elm.Module
removeDeclaration name =
    checkRemovePortModifier . filterDeclarations ((/=) name)


checkRemovePortModifier :: Elm.Module -> Elm.Module
checkRemovePortModifier =
    over Elm.moduleEffects $ \case
        Elm.Ports [] ->
            Elm.NoEffects

        other ->
            other


filterDeclarations :: (Name.Name -> Bool) -> Elm.Module -> Elm.Module
filterDeclarations predicate =
    let
        getDeclName =
            view (Elm.valueOfLocated . Elm.declarationName . Elm.valueOfLocated)

        shouldKeepExposing = \case
            Src.Lower name ->
                predicate name

            _ ->
                True
    in
    over Elm.moduleDeclarations
        (filter (predicate . getDeclName))
    . over (Elm.moduleExports . Elm.valueOfLocated . Elm._Explicit)
        (filter (shouldKeepExposing . view Elm.valueOfLocated))
    . over (Elm.moduleEffects . Elm._Ports)
        (filter (predicate . view (Elm.portName . Elm.valueOfLocated)))


-- Reduction using DDmin:


type DDState = DD.State Name.Name


reduceDDmin :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> Module.Raw -> IO DDState
reduceDDmin phaseName projectDir testCase reductionReport moduleName = do
    summary <- Elm.parseProject projectDir

    ModuleDependencies.Local modulePath <- ModuleDependencies.pathFromModuleName summary moduleName

    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        removeDeclarations removedNames =
            filterDeclarations (`notElem` removedNames) module_
                & AST.translateModule
                & AST.renderModule
                & Text.writeFile modulePath

        description redundantDeclarations =
            Reduce.Description
                { shortDescription = "Removed " ++ show (length redundantDeclarations) ++ " declaration(s)"
                , longDescription = unlines ("Removed:" : map show redundantDeclarations)
                }

        allDeclarationNames =
            toListOf (Elm.moduleDeclarations . traverse . Elm.valueOfLocated . Elm.declarationName . Elm.valueOfLocated) module_

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = DD.stateVariants
            , applyVariant = removeDeclarations . Reduce.delta
            , variantDescription = description . Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        (DD.State allDeclarationNames 2)



-- Reduce elm modules:


reduceElmModule :: (Elm.Module -> Reductions Elm.Module) -> String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceElmModule reductionVariants phaseName projectDir testCase reductionReport modulePath = do
    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        applyReduced =
            Text.writeFile modulePath
                . AST.renderModule
                . AST.translateModule

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = map (uncurry Reduce.Variant) . Reduce.reductions . reductionVariants
            , applyVariant = applyReduced . Reduce.state
            , variantDescription = Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        module_


reducePattern :: Src.Pattern -> Reductions Src.Pattern
reducePattern pattern =
    let
        wildcardReduction original =
            Reduce.reduction original Pattern.Anything
                "Reduced pattern match to wildcard"
                (unlines
                    [ "Pattern"
                    , AST.prettyPattern pattern
                    , "replaced with _"
                    ]
                )

        listPatternReduction patternRemoved =
            Reduce.Description
                { shortDescription = "Reduced list pattern element"
                , longDescription = unlines
                    [ "Removed pattern match"
                    , AST.prettyPattern patternRemoved
                    , "from"
                    , AST.prettyPattern pattern
                    ]
                }

        recordPatternReduction nameToRemove =
            Reduce.Description
                { shortDescription = "Removed record pattern destructing name " <> show (view Elm.valueOfLocated nameToRemove)
                , longDescription = unlines
                    [ "From the pattern:"
                    , AST.prettyPattern pattern
                    ]
                }

        patternReductions =
            wildcardReduction
            <> Pattern._List (Reduce.removeElements (Just . listPatternReduction) <> traverse reducePattern)
            <> Pattern._Ctor (_3 (traverse reducePattern))
            <> Pattern._CtorQual (_4 (traverse reducePattern))
            <> Pattern._Record (Reduce.removeElements (Just . recordPatternReduction))
            <> Pattern._Tuple (_1 reducePattern <> _2 reducePattern <> _3 (traverse reducePattern))
    in
    Pattern.asF (Elm.valueOfLocated patternReductions) pattern


reduceCases :: Elm.Expr -> Reductions Elm.Expr
reduceCases expr =
    let
        removalDescription (pattern, _) =
            Reduce.Description
                { shortDescription = "Removing case pattern match"
                , longDescription = unlines
                    [ "Removed pattern match on"
                    , AST.prettyPattern pattern
                    ]
                }

        casesReductions =
            -- either remove a case pattern match completely
            Reduce.removeElements (Just . removalDescription)
            -- or reduce a case pattern itself
            <> traverse (_1 reducePattern)

        caseOfReductions =
            Expr._Case (_2 casesReductions)
    in
    Expr.asF (Elm.valueOfLocated caseOfReductions) expr


reduceLets :: Elm.Expr -> Reductions Elm.Expr
reduceLets expr =
    let
        removalDescription = \case
            Expr.Define def ->
                Reduce.Description
                    { shortDescription = "Removing let definition " <> Name.toString (view (Elm.letDefName . Elm.valueOfLocated) def)
                    , longDescription = unlines
                        [ "Removed definition:"
                        , AST.prettyExpr (view Elm.letDefBody def)
                        ]
                    }

            Expr.Destruct destr ->
                Reduce.Description
                    { shortDescription = "Removing let destruction " <> AST.prettyPattern (view Elm.letDestructPattern destr)
                    , longDescription = unlines
                        [ "Removed definition:"
                        , AST.prettyExpr (view Elm.letDestructBody destr)
                        ]
                    }

        removeDefs = \case
            [x] ->
                pure [x]

            ls ->
                Reduce.removeElements (Just . removalDescription) ls
    in
    Expr.asF (Elm.valueOfLocated (Expr._Let (_1 removeDefs))) expr


reduceArguments :: Elm.Decl -> Reductions Elm.Decl
reduceArguments decl =
    let
        removeArgument arg =
            Reduce.Description
                { shortDescription =
                    "Removed argument "
                    <> AST.prettyPattern arg
                    <> " from declaration "
                    <> Name.toString (view (Elm.declarationName . Elm.valueOfLocated) decl)
                , longDescription = ""
                }

    in
    Elm.declarationArguments (Reduce.removeElements (Just . removeArgument)) decl


subexpressionReductions :: Elm.Expr -> Reductions Elm.Expr
subexpressionReductions expr =
    let
        subpartReduction subexpression =
            [   ( subexpression
                , Reduce.Description
                    { shortDescription = "Reduced expression to one of its subparts"
                    , longDescription = unlines
                        [ "Before:"
                        , AST.prettyExpr expr
                        , ""
                        , "After:"
                        , AST.prettyExpr subexpression
                        ]
                    }
                )
            ]

        exprF =
            view Elm.valueOfLocated (Expr.deconstruct expr)
    in
    Reduce.Reductions expr (foldMap subpartReduction exprF)


reduceRecursively :: (Elm.Expr -> Reductions Elm.Expr) -> Elm.Expr -> Reductions Elm.Expr
reduceRecursively reduceFlat expr =
    let
        onOriginal =
            Expr.construct . (fmap . fmap) snd

        onRecursion =
            (fmap . fmap) fst
    in
    -- TODO: this is not explicitly recursive. But is it better?
    Expr.foldWithOriginal
        (\exprF ->
            reduceFlat (onOriginal exprF)
            <> fmap Expr.construct ((traverse . traverse) id (onRecursion exprF))
        )
        expr


reduceToSubpartsInModule :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceToSubpartsInModule =
    reduceElmModule (Elm.moduleDeclarations (traverse (Elm.valueOfLocated (Elm.declarationBody (reduceRecursively subexpressionReductions)))))


reduceCasesInModule :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceCasesInModule =
    reduceElmModule (Elm.moduleDeclarations (traverse (Elm.valueOfLocated (Elm.declarationBody (reduceRecursively reduceCases)))))


reduceLetsInModule :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceLetsInModule =
    reduceElmModule (Elm.moduleDeclarations (traverse (Elm.valueOfLocated (Elm.declarationBody (reduceRecursively reduceLets)))))


reduceArgumentsInModule :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceArgumentsInModule =
    reduceElmModule (Elm.moduleDeclarations (traverse (Elm.valueOfLocated reduceArguments)))