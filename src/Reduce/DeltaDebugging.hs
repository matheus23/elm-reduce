{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

module Reduce.DeltaDebugging where

import Control.Lens hiding ((<.>), from)
import qualified Reduce

data State a = State
    { remaining :: [a]
    , splitInto :: Int
    }

{-|
    λ> groupsOf 3 [1..10]
    [[1,2,3],[4,5,6],[7,8,9],[10]]
-}
groupsOf :: Int -> [a] -> [[a]]
groupsOf size ls =
    case splitAt size ls of
        (splitOff, []) ->
            [splitOff]

        (splitOff, remainingElems) ->
            splitOff : groupsOf size remainingElems

{-|
    λ> elemRemoveVariants [1..5]
    [(1,[2,3,4,5]),(2,[1,3,4,5]),(3,[1,2,4,5]),(4,[1,2,3,5]),(5,[1,2,3,4])]
-}
elemRemoveVariants :: [a] -> [(a, [a])]
elemRemoveVariants = \case
    [] ->
        []

    (x:xs) ->
        -- either leave the first element out
        -- or keep the first element and leave any other out
        (x, xs) : over (mapped . _2) ((:) x) (elemRemoveVariants xs)


stateVariants :: State a -> [Reduce.Variant (State a) [a]]
stateVariants reductionState@State{ remaining, splitInto } =
    let
        remainingDeclsLength =
            length remaining

        chunkSize =
            remainingDeclsLength `div` splitInto
    in
    if chunkSize < 1 then
        []
    else
        let
            possiblyRedundantGroups =
                groupsOf chunkSize remaining

            reducedState (redundantGroup, nonRedundantGroups) =
                Reduce.Variant
                    { delta = redundantGroup
                    , state =
                        let
                            nonRedundantDecls =
                                concat nonRedundantGroups
                        in
                        State
                            { remaining = nonRedundantDecls
                            , splitInto = 2
                            }
                    }
        in
        map reducedState (elemRemoveVariants possiblyRedundantGroups)
            ++ stateVariants reductionState { splitInto = splitInto * 2 }
