{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}

module Reduce.ImportNormalization.FullyQualifyIdentifiers where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Name as Name

-- misc
import qualified Data.Map as Map
import Data.Map (Map)
import Control.Lens hiding ((<.>))

import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm as Elm
import qualified Language.Elm.Rename as Rename
import Language.Elm.OrphanInstances ()
import Reduce.ImportNormalization.Types


inModule :: Map Identifier Name.Name -> Elm.Module -> Elm.Module
inModule importDict module_ =
    let
        -- throws away any identifiers that are exposed into module top-level scope,
        -- while adding all operators that were defined in the module
        -- for example `exposing (view, something, (|.))` becomes `exposing ((|.), (|=))`
        normalizeImport (Src.Import importedModule _ _) =
            let
                binops =
                    importDict
                        & Map.mapMaybeWithKey (filterBinopFromModule (view Elm.valueOfLocated importedModule))
                        & Map.elems

                exposeBinops =
                    map (Elm.dummyLocation . Src.Operator) binops
            in
            Src.Import importedModule Nothing (Src.Explicit exposeBinops)

        filterBinopFromModule moduleName (Op name) importedModule
            | moduleName == importedModule =
                Just name
        filterBinopFromModule _ _ _ =
            Nothing

        qualifyingRenamer = \case
            Id ns qualifier name ->
                case importDict Map.!? Id ns qualifier name of
                    Just importedModule ->
                        (Just importedModule, name)
                    
                    Nothing ->
                        (qualifier, name)
            
            Op name ->
                (Nothing, name)

            ListId ->
                (Nothing, "List")
    in
    module_
        & Rename.inModule (Rename.renameNonshadowed qualifyingRenamer)
        & over (Elm.moduleImports . mapped) normalizeImport
