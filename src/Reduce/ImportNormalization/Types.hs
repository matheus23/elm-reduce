{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE LambdaCase #-}

-- TODO: The ImportNormalization module structure should be rebuilt
-- this is not only relevant to import normalization (see DefinitionDependencies)
module Reduce.ImportNormalization.Types where

import qualified Elm.Name as Name
import Language.Elm.OrphanInstances ()


data IdentifierDeclaration
    = UnionType Name.Name
    | AliasType Name.Name
    | UnionConstructor Name.Name Name.Name
    | AliasConstructor Name.Name
    | FunctionDeclaration Name.Name
    | OperatorDeclaration Name.Name
    deriving (Show, Eq, Ord)


data Identifier
    = Id Namespace (Maybe Name.Name) Name.Name
    | Op Name.Name
    | ListId -- `List` type identifiers are a special case in the elm compiler.
    deriving (Show, Eq, Ord)


data Namespace
    = TypeNamespace | FunctionNamespace
    deriving (Show, Eq, Ord)


declarationToIdentifier :: Maybe Name.Name -> IdentifierDeclaration -> Identifier
declarationToIdentifier qualifier = \case
    UnionType name ->
        Id TypeNamespace qualifier name

    AliasType name ->
        Id TypeNamespace qualifier name

    UnionConstructor _ name ->
        Id FunctionNamespace qualifier name

    AliasConstructor name ->
        Id FunctionNamespace qualifier name

    FunctionDeclaration name ->
        Id FunctionNamespace qualifier name

    OperatorDeclaration name ->
        Op name
