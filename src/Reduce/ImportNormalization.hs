{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}

module Reduce.ImportNormalization where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Compiler.Module as Module
import qualified Elm.Project.Summary as Summary
import qualified "elm" Reporting.Annotation as Ann

-- misc
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Maybe as Maybe
import Control.Lens

import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.ModuleDependencies as ModuleDependencies
import Language.Elm.OrphanInstances ()
import Reduce.ImportNormalization.Types
import qualified Reduce.ImportNormalization.FullyQualifyIdentifiers as FullyQualifyIdentifiers


normalizeImports :: Summary.Summary -> Elm.Module -> IO Elm.Module
normalizeImports summary module_ = do
    importDict <- moduleImportDict summary module_
    return (FullyQualifyIdentifiers.inModule importDict module_)


moduleImportDict :: Summary.Summary -> Elm.Module -> IO (Map Identifier Module.Raw)
moduleImportDict summary module_ =
    let
        locallyDefinedIdentifiers =
            map (declarationToIdentifier Nothing) (moduleExports module_)

        notShadowedByLocalDefinitions identifier _ =
            identifier `notElem` locallyDefinedIdentifiers

        symbolsFromImport import_ =
                -- It is important, that local definitions shadow imported definitions!
                Map.filterWithKey notShadowedByLocalDefinitions
                . importDictFromIdentifiers import_
                . moduleExports
                <$> ModuleDependencies.parseProjectModule summary (view (Elm.importName . Elm.valueOfLocated) import_)
    in
    Map.unions <$> mapM symbolsFromImport (view Elm.moduleImports module_)


importDictFromIdentifiers :: Src.Import -> [IdentifierDeclaration] -> Map Identifier Module.Raw
importDictFromIdentifiers (Src.Import locatedImportName alias exposings) identifierDeclarations =
    let
        importName =
            Ann.toValue locatedImportName

        qualifier =
            Maybe.fromMaybe importName alias

        -- TODO: Decide, whether or not we assume type-correct code.
        -- We don't check for duplicate names/definitions here!

        exportedQualified =
            Map.fromList
                [ (identifier, importName)
                | identifierDeclaration <- identifierDeclarations
                , let identifier = declarationToIdentifier (Just qualifier) identifierDeclaration
                ]

        exportedUnqualified =
            Map.fromList
                [ (identifier, importName)
                | identifierDeclaration <- identifierDeclarations
                , filterExposing exposings identifierDeclaration
                , let identifier = declarationToIdentifier Nothing identifierDeclaration
                ]
    in
    Map.union exportedQualified exportedUnqualified


moduleExports :: Elm.Module -> [IdentifierDeclaration]
moduleExports module_ =
    moduleDefinedIdentifiers module_
        & filter (filterExposing (view (Elm.moduleExports . Elm.valueOfLocated) module_))


filterExposing :: Src.Exposing -> IdentifierDeclaration -> Bool
filterExposing Src.Open _ = True
filterExposing (Src.Explicit locatedNames) identifier =
    toListOf (traverse . Elm.valueOfLocated) locatedNames
        & any (isIdentifierExposedVia identifier)


isIdentifierExposedVia :: IdentifierDeclaration -> Src.Exposed -> Bool
isIdentifierExposedVia identifier exposed =
    case (exposed, identifier) of
        (Src.Upper name _, UnionType typeName)
            | name == typeName ->
                True

        (Src.Upper name Src.Public, UnionConstructor typeName _)
            | name == typeName ->
                True

        (Src.Upper name _, AliasType typeName)
            | name == typeName ->
                True

        (Src.Upper name _, AliasConstructor constructorName)
            | name == constructorName ->
                True

        (Src.Lower name, FunctionDeclaration functionName)
            | name == functionName ->
                True

        (Src.Operator name, OperatorDeclaration operatorName)
            | name == operatorName ->
                True

        _ ->
            False


moduleDefinedIdentifiers :: Elm.Module -> [IdentifierDeclaration]
moduleDefinedIdentifiers =
    moduleDefinedDeclarationIdentifiers
        <> moduleDefinedAliasIdentifiers
        <> moduleDefinedUnionIdentifiers
        <> moduleDefinedOperatorIdentifiers
        <> moduleDefinedPortIdentifiers


moduleDefinedDeclarationIdentifiers :: Elm.Module -> [IdentifierDeclaration]
moduleDefinedDeclarationIdentifiers module_ =
    toListOf (Elm.moduleDeclarations . traverse . Elm.valueOfLocated . Elm.declarationName . Elm.valueOfLocated) module_
        & map FunctionDeclaration


moduleDefinedPortIdentifiers :: Elm.Module -> [IdentifierDeclaration]
moduleDefinedPortIdentifiers module_ =
    FunctionDeclaration <$>
        toListOf (Elm.moduleEffects . Elm._Ports . traverse . Elm.portName . Elm.valueOfLocated) module_


moduleDefinedAliasIdentifiers :: Elm.Module -> [IdentifierDeclaration]
moduleDefinedAliasIdentifiers module_ =
    toListOf (Elm.moduleAliases . traverse) module_
        & concatMap typeAliasDefinedIdentifiers


typeAliasDefinedIdentifiers :: Elm.Alias -> [IdentifierDeclaration]
typeAliasDefinedIdentifiers alias =
    let
        name =
            view (Elm.aliasName . Elm.valueOfLocated) alias
    in
    case view (Elm.aliasType . Elm.valueOfLocated) alias of
        -- Type aliases for records automatically generate constructors for the records
        Src.TRecord{} ->
            [ AliasType name, AliasConstructor name ]
    
        _ ->
            [ AliasType name ]


moduleDefinedUnionIdentifiers :: Elm.Module -> [IdentifierDeclaration]
moduleDefinedUnionIdentifiers module_ =
    toListOf (Elm.moduleUnions . traverse) module_
        & concatMap (\union -> unionDefinedIdentifiers union)


moduleDefinedOperatorIdentifiers :: Elm.Module -> [IdentifierDeclaration]
moduleDefinedOperatorIdentifiers module_ =
    toListOf (Elm.moduleOperators . traverse . Elm.binopName) module_
        & map OperatorDeclaration


unionDefinedIdentifiers :: Elm.Union -> [IdentifierDeclaration]
unionDefinedIdentifiers union =
    let
        unionName =
            view (Elm.unionName . Elm.valueOfLocated) union

        definedConstructors =
            toListOf (Elm.unionConstructors . traverse . _1 . Elm.valueOfLocated) union
                & map (UnionConstructor unionName)
    in
    UnionType unionName : definedConstructors
