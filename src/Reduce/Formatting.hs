{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE DuplicateRecordFields #-}

module Reduce.Formatting where

-- Elm compiler
import qualified Elm.Project.Summary as Summary
import qualified Elm.Package as Pkg

-- misc
import qualified Data.Text.IO as Text

import qualified Reduce
import qualified Reduce.Transfer as Transfer
import qualified Language.Elm.AST as AST
import qualified Language.Elm as Elm
    

type FormattingState =
    ( [FilePath], [FilePath] )


reduce :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> IO FormattingState
reduce phaseName projectRoot testCase reductionReport =
    let
        formatFile filepath = do
            Right module_ <- Elm.parseModule Pkg.dummyName <$> Text.readFile filepath
            Text.writeFile filepath (AST.renderModule (AST.translateModule module_))

        description (Reduce.Variant (unformatted, formatted) toBeFormatted) =
            Reduce.Description
                { shortDescription = "Formatting " ++ show toBeFormatted
                , longDescription =
                    unlines
                        [ "Unformatted:"
                        , (unlines . map show) unformatted
                        , ""
                        , "Formatted:"
                        , (unlines . map show) (toBeFormatted:formatted)
                        ]
                }
    in do
        summary <- Elm.parseProject projectRoot
        elmFiles <- Elm.getElmFiles projectRoot (Summary._project summary)

        Reduce.greedily
            Reduce.Reduction
                { phaseName = phaseName
                , variantsOf = Transfer.oneByOneVariants
                , applyVariant = formatFile . Reduce.delta
                , variantDescription = description
                }
            testCase
            reductionReport
            projectRoot
            (elmFiles, [])
