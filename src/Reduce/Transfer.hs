module Reduce.Transfer where

import qualified Data.Map as Map
import Data.Map (Map)

import qualified Reduce

-- λ> oneByOne [1,2,3] [4,5,6]
-- [([2,3],[1,4,5,6]),([1,3],[2,4,5,6]),([1,2],[3,4,5,6])]
oneByOne :: [a] -> [a] -> [([a], [a])]
oneByOne source destination =
    map Reduce.state (curry oneByOneVariants source destination)


oneByOneVariants :: ([a], [a]) -> [Reduce.Variant ([a], [a]) a]
oneByOneVariants ([], _) = []
oneByOneVariants ((x:xs), destination) =
    Reduce.Variant
        { Reduce.delta = x
        , Reduce.state = (xs, x:destination)
        }
        : map (Reduce.mapState appendX) (oneByOneVariants (xs, destination))
    where appendX (source', destination') = (x:source', destination')


oneByOneMap :: Ord k => Map k v -> Map k v -> [((k, v), Map k v, Map k v)]
oneByOneMap mapA mapB = map toMaps (oneByOne (Map.toList mapA) (Map.toList mapB))
    where toMaps (assocListA, assocListB) = (head assocListB, Map.fromList assocListA, Map.fromList assocListB)
