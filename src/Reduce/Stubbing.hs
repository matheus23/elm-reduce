{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

module Reduce.Stubbing where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Package as Pkg
import qualified Elm.Name as Name
import qualified "elm" Reporting.Annotation as Ann
import qualified "elm" Reporting.Region as Region

-- misc
import qualified Data.Text.IO as Text
import Control.Lens

import qualified Language.Elm as Elm
import qualified Language.Elm.Lenses as Elm
import qualified Language.Elm.AST as AST

import qualified Reduce
import qualified Reduce.Transfer as Transfer


-- let undefinedValue_ = undefinedValue_ in undefinedValue_
letXinX :: Elm.Expr
letXinX =
    let
        xName =
            Name.fromText "undefinedValue_"

        x =
            unlocated (Elm.Var Src.Value xName)

        applyX =
            unlocated (Elm.Call x [unlocated Elm.Unit])

        bogusLocation =
            Region.Region (Region.Position 0 0) (Region.Position 0 0)

        unlocated =
            Ann.at (Region.Position 0 0) (Region.Position 0 0)
    in
    unlocated
        (Elm.Let
            [ Elm.Define bogusLocation (unlocated xName) [unlocated Src.PAnything] applyX Nothing ]
            applyX
        )


-- TODO: Remove arguments that are now obviously unused, or do that in a later pass?
reduce :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO ([Name.Name], [Name.Name])
reduce phaseName projectRoot testCase reductionReport modulePath = do
    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        allDeclarations =
            toListOf (Elm.moduleDeclarations . traverse . Elm.valueOfLocated . Elm.declarationName . Elm.valueOfLocated) module_

        description (Reduce.Variant (nonStubbed, alreadyStubbed) justStubbed) =
            Reduce.Description
                { shortDescription = "Stubbed definition " ++ show justStubbed
                , longDescription = (unlines . concat)
                    [ [ "Stubbed Definitions:"]
                    , map show alreadyStubbed
                    , [ "Non-stubbed Definitions:"]
                    , map show nonStubbed
                    ]
                }

        modifyDecl nameToStub decl =
            if view (Elm.valueOfLocated . Elm.declarationName . Elm.valueOfLocated) decl == nameToStub then
                set (Elm.valueOfLocated . Elm.declarationBody) letXinX decl
            else
                decl

        stubDeclaration declarationNameToStub =
            Elm.parseModuleFromFile Pkg.dummyName modulePath
            <&> over (Elm.moduleDeclarations . mapped) (modifyDecl declarationNameToStub)
            <&> AST.translateModule
            <&> AST.renderModule
            >>= Text.writeFile modulePath

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = Transfer.oneByOneVariants
            , applyVariant = stubDeclaration . Reduce.delta
            , variantDescription = description
            }
        testCase
        reductionReport
        projectRoot
        (allDeclarations, [])
