{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Reduce.Imports where

-- Elm format
import qualified AST.Module as ElmFormat
import qualified AST.V0_16 as ElmFormat
import qualified ElmFormat.Parse as ElmFormat

-- misc
import qualified Data.List as List
import qualified Data.Text.IO as Text
import Control.Lens

import qualified Language.Elm.AST as AST
import Language.Elm.OrphanInstances ()
import qualified Reduce
import Reduce.Reductions (Reductions(..))
import qualified Reduce.Reductions as Reduce


reduceImportsInModule :: ElmFormat.Module -> Reductions ElmFormat.Module
reduceImportsInModule =
    let
        makeImportName =
            concat . List.intersperse "." . map (\(ElmFormat.UppercaseIdentifier i) -> i)

        description importIds _ =
            Just Reduce.Description
                { shortDescription = "Removed import " ++ show (makeImportName importIds)
                , longDescription = ""
                }

        -- lens into ElmFormat.Module imports
        moduleImports f module_ =
            fmap (\imports' -> module_ { ElmFormat.imports = imports' }) (f (ElmFormat.imports module_))
    in
    moduleImports (_2 (Reduce.removeMapElements description))


reduce :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO ElmFormat.Module
reduce phaseName projectDir testCase reductionReport modulePath = do
    content <- Text.readFile modulePath

    parsed <-
        maybe (error "Could not parse module file for import reduction") return
            (ElmFormat.toMaybe (ElmFormat.parse content))

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = map (uncurry Reduce.Variant) . Reduce.reductions . reduceImportsInModule
            , applyVariant = Text.writeFile modulePath . AST.renderModule . Reduce.state
            , variantDescription = Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        parsed
