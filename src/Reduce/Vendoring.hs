{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Reduce.Vendoring where

import qualified Reporting.Task as Task
import qualified Reporting.Progress.Terminal as Terminal
import qualified Elm.Project.Json as Json
import qualified Elm.Project.Summary as Summary
import qualified Elm.Compiler.Module as Module
import qualified Elm.Package as Package
import qualified Elm.PerUserCache
import qualified Json.Encode

import System.FilePath
import System.Directory
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.ByteString.Builder as ByteString.Builder
import qualified Data.ByteString as ByteString
import qualified Data.ByteString.Lazy as ByteString.Lazy
import Data.Function ((&))
import Data.Foldable (traverse_)
import Data.List
import Control.Lens hiding ((<.>))

import FsUtils
import Language.Elm.OrphanInstances ()
import qualified Language.Elm as Elm
import qualified Language.Elm.ModuleDependencies as ModuleDependencies
import qualified Language.Elm.Lenses as Elm
import qualified Reduce.Transfer as Transfer
import qualified Reduce

import Reduce.Reductions (Reductions(..))
import qualified Reduce.Reductions as Reduce


-- Perform arbitrary reduction steps over project files iteratively


reduceViaProjectFile :: (Json.Project -> [Reduce.Variant Json.Project Reduce.Description]) -> String -> IO Reduce.Interestingness -> IO String-> FilePath -> Json.Project -> IO Json.Project
reduceViaProjectFile reduceProject phaseName testCase reductionReport projectRoot failingProject =
    let
        writeProject project = do
            let projectBS = ByteString.Lazy.toStrict (ByteString.Builder.toLazyByteString (Json.Encode.encode (Json.encode project)))
            ByteString.writeFile (projectRoot </> "elm" <.> "json") projectBS
    in
    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = reduceProject
            , applyVariant = writeProject . Reduce.state
            , variantDescription = Reduce.delta
            }
        testCase
        reductionReport
        projectRoot
        failingProject



-- Try removing unused dependencies:


reduceDependencies :: String -> Module.Raw -> FilePath -> IO Reduce.Interestingness -> IO String -> IO Json.Project
reduceDependencies phaseName mainModule projectDir testCase reductionReport = do
    initialSummary <- Elm.parseProject projectDir
    modulePaths <- Map.keys <$> ModuleDependencies.localDependencies mainModule initialSummary
    absoluteSourceDirectories <-
        traverse (makeAbsolute . (projectDir </>)) (toListOf (Elm.summaryProject . Elm._AppInfo . Elm.appSourceDirs . traverse) initialSummary)
    absoluteModulePaths <-
        traverse makeAbsolute modulePaths
    let
        -- Note: We have to make sure that the source-directories don't get reduced to nothing, so that
        -- it would be impossible for other passes to reduce the Main file, since it can't be resolved anymore
        -- (It is possible to remove the main source directory without invalidating the test case)
        extendedTestCase = testCase <&> \case
            Reduce.Uninteresting ->
                Reduce.Uninteresting

            Reduce.Interesting ->
                if all (\absoluteModulePath -> any (`isPrefixOf` absoluteModulePath) absoluteSourceDirectories) absoluteModulePaths then
                    Reduce.Interesting
                else
                    Reduce.Uninteresting

    reduceViaProjectFile dependencyReduction phaseName extendedTestCase reductionReport projectDir (Summary._project initialSummary)


dependencyReduction :: Json.Project -> [Reduce.Variant Json.Project Reduce.Description]
dependencyReduction =
    let
        removeSourceDirDescription dir =
            Reduce.Description
                { shortDescription = "Removed source-directory " ++ show dir
                , longDescription = ""
                }

        removeDepsDescription depKind dep version =
            Reduce.Description
                { shortDescription = "Removed " ++ depKind ++ " dependency: " ++ show dep ++ ":" ++ show version
                , longDescription = ""
                }

        jsonProjectReductions =
            Elm._AppInfo $
                Elm.appSourceDirs (Reduce.removeElements (Just . removeSourceDirDescription))
                <> Elm.appDepsDirect (Reduce.removeMapElements (\k v -> Just (removeDepsDescription "direct" k v)))
                <> Elm.appDepsTransitive (Reduce.removeMapElements (\k v -> Just (removeDepsDescription "transitive" k v)))
    in
    fmap (uncurry Reduce.Variant) . reductions . jsonProjectReductions



-- Vendor dependencies


dependencyVendoring :: String -> IO Reduce.Interestingness -> IO String -> FilePath -> Json.Project -> IO Json.Project
dependencyVendoring phaseName testCase reductionReport projectRoot project = do
    let
        description package =
            Reduce.Description
                { shortDescription = "Vendored " ++ show package
                , longDescription = ""
                }

    -- we can't vendor all packages. copyDirectDependency filters out:
    -- * Kernel packages (that is packages that have an "elm" namespace), those are the only packages allowed to write kernel code (direct javascript calls)
    -- * Packages which have dependencies from the transitive dependencies that could not be moved to direct dependencies
    vendorableDependencies <- copyDirectDependencies projectRoot project
    resultProjectFile <- reduceViaProjectFile
        (fmap (Reduce.mapDelta description) .  vendorProjectDirectDependencies vendorableDependencies)
        phaseName
        testCase
        reductionReport
        projectRoot
        project

    -- we make sure, that git keeps all our source directories, since we can't compile otherwise.
    -- git wouldn't restore them when we have no files in them, which happens,
    -- because we delete all .elm files inside of them, when they're not referenced anymore. (See Reduce.MergeModules.reduceDeadModules)
    traverse_ gitKeep (toListOf (Elm._AppInfo . Elm.appSourceDirs . traverse) resultProjectFile)
    return resultProjectFile


vendoringLibsDirectory :: FilePath
vendoringLibsDirectory = "vendored-libs"


gitKeep :: FilePath -> IO ()
gitKeep path =
    writeFile (path </> ".gitkeep") ""


-- vendor direct dependencies by refering to their source directly via "source-directories"
vendorProjectDirectDependencies :: Map Package.Name Package.Version -> Json.Project -> [Reduce.Variant Json.Project Package.Name]
vendorProjectDirectDependencies vendorableDependencies = \case
    (Json.App appInfo) ->
        vendorableDependencies
            & Map.intersection (Json._app_deps_direct appInfo)
            & Map.toList
            & map
                (\(package, version) ->
                    let
                        projectJson =
                            Json.App appInfo
                                { Json._app_source_dirs =
                                    -- seems like elm libraries always have their sources in the "src" directory
                                    (vendoringLibsDirectory </> packagePath package version </> "src")
                                        : Json._app_source_dirs appInfo
                                , Json._app_deps_direct = Map.delete package (Json._app_deps_direct appInfo)
                                }
                    in
                    Reduce.Variant
                        { state = projectJson
                        , delta = package
                        }
                )

    _ ->
        error "TODO: elm package projects not supported yet"


copyDirectDependencies :: FilePath -> Json.Project -> IO (Map Package.Name Package.Version)
copyDirectDependencies projectRoot project =
    let
        vendoredLibsDir = projectRoot </> vendoringLibsDirectory
        dependencies = directDependencies project
    in do
        reporter <- Terminal.create
        createDirectoryIfMissing True vendoredLibsDir
        packageRoot <- Elm.PerUserCache.getPackageRoot
        dependencies
            & Map.traverseMaybeWithKey
                (\package version ->
                    -- filter kernel packages, since those have direct javascript calls, which are only allowed in elm libraries, therefore would fail to vendor
                    if Package.isKernel package then
                        return Nothing
                    else do
                        let pkg = packagePath package version
                        result <- Task.try reporter (Json.read (packageRoot </> pkg </> "elm.json"))
                        case result of
                            Just (Json.Pkg packageProject) -> do
                                let libraryDeps = Set.fromList (Map.keys (Json._pkg_deps packageProject))
                                let projectTransitiveDeps = Set.fromList (Map.keys (transitiveDependencies project))
                                let depsIntersection = Set.intersection libraryDeps projectTransitiveDeps
                                -- if the library depends on one of our transitive dependencies, then we can't vendor it
                                if not (Set.null depsIntersection) then do
                                    return Nothing
                                else do
                                    copyDir (packageRoot </> pkg) (vendoredLibsDir </> pkg)
                                    return (Just version)

                            _ -> do
                                return Nothing

                )


packagePath :: Package.Name -> Package.Version -> FilePath
packagePath package version = Package.toFilePath package </> Package.versionToString version


directDependencies :: Json.Project -> Map Package.Name Package.Version
directDependencies (Json.App app) = Json._app_deps_direct app
directDependencies _ = error "TODO: elm package projects not supported yet"


transitiveDependencies :: Json.Project -> Map Package.Name Package.Version
transitiveDependencies (Json.App app) = Json._app_deps_trans app
transitiveDependencies _ = error "TODO: elm package projects not supported yet"



-- Reduce Indirect dependencies


indirectDependenciesReduction :: String -> IO Reduce.Interestingness -> IO String -> FilePath -> Json.Project -> IO Json.Project
indirectDependenciesReduction =
    let
        description package =
            Reduce.Description
                { shortDescription = "Move to direct dependency: " ++ show package
                , longDescription = ""
                }
    in
    reduceViaProjectFile
        (fmap (Reduce.mapDelta description) . reduceProjectIndirectDependencies)


dependenciesToString :: Json.Project -> String
dependenciesToString (Json.App app) = show (sort (Map.toList (Json._app_deps_trans app)))
dependenciesToString _ = error "TODO: elm package projects not supported yet"


reduceProjectIndirectDependencies :: Json.Project -> [Reduce.Variant Json.Project Package.Name]
reduceProjectIndirectDependencies = \case
    (Json.App appInfo) ->
        (map . Reduce.mapState) Json.App (reduceAppInfoIndirectDependencies appInfo)

    _ ->
        error "TODO: elm package projects not supported yet"


reduceAppInfoIndirectDependencies :: Json.AppInfo -> [Reduce.Variant Json.AppInfo Package.Name]
reduceAppInfoIndirectDependencies (Json.AppInfo version sourceDirs depsDirect depsTrans testDeps testDepsTrans) =
    let
        makeAppInfo ((package, _), depsTrans', depsDirect') =
            let
                appInfo =
                    Json.AppInfo version sourceDirs depsDirect' depsTrans' testDeps testDepsTrans
            in
            Reduce.Variant
                { state = appInfo
                , delta = package
                }
    in
    map makeAppInfo (Transfer.oneByOneMap depsTrans depsDirect)
