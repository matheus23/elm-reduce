{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}

module Reduce.TypeDeclarations where

-- Elm compiler
import qualified AST.Valid as Elm
import qualified AST.Source as Src
import qualified Elm.Package as Pkg
import qualified Elm.Name as Name
import qualified "elm" Reporting.Annotation as Ann

-- misc
import Control.Lens hiding ((<.>), from)
import qualified Data.Text.IO as Text
import qualified Data.Text as Text
import qualified Data.Char as Char
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Map (Map)
import Data.Set (Set)

import qualified Language.Elm as Elm
import qualified Language.Elm.Traverse.Type as Type
import qualified Language.Elm.Lenses as Elm
import Language.Elm.OrphanInstances ()
import qualified Language.Elm.DefinitionDependencies as DefinitionDependencies
import qualified Language.Elm.AST as AST

import qualified Reduce
import qualified Reduce.Transfer as Transfer
import qualified Reduce.Reductions as Reduce
import Reduce.Reductions (Reductions(..))
import Reduce.Definitions (reduceElmModule)

import Graph (Graph)


-- TODO: Abstract and unify with Reduce.Declarations! (maybe via a generalized Elm.Module reducer?)
reduceDead :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO (Graph Name.Name)
reduceDead phaseName projectDir testCase reductionReport modulePath = do
    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        typeDepGraph =
            DefinitionDependencies.typeDependencyGraph module_

        description deletedFunction =
            Reduce.Description
                { shortDescription = "Deleted type declaration " ++ show deletedFunction
                , longDescription = ""
                }

        deleteName nameToBeDeleted =
            removeTypeDeclarationInFile nameToBeDeleted modulePath

        isUppercaseIdentifier =
            Text.any Char.isLower . Text.take 1 . Name.toText

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = Reduce.deadReferencesVariants isUppercaseIdentifier
            , applyVariant = deleteName . Reduce.delta
            , variantDescription = description . Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        typeDepGraph


applyFileTransform :: (Elm.Module -> Elm.Module) -> FilePath -> IO ()
applyFileTransform fileTransform path =
    Elm.parseModuleFromFile Pkg.dummyName path
    <&> fileTransform
    <&> AST.translateModule
    <&> AST.renderModule
    >>= Text.writeFile path


removeTypeDeclarationInFile :: Name.Name -> FilePath -> IO ()
removeTypeDeclarationInFile name =
    applyFileTransform (removeTypeDeclaration name)


removeTypeDeclaration :: Name.Name -> Elm.Module -> Elm.Module
removeTypeDeclaration name =
    filterTypeDeclarations ((/=) name)


filterTypeDeclarations :: (Name.Name -> Bool) -> Elm.Module -> Elm.Module
filterTypeDeclarations predicate =
    let
        shouldKeepExposing = \case
            Src.Upper name _ ->
                predicate name

            _ ->
                True
    in
    over Elm.moduleAliases
        (filter (predicate . view (Elm.aliasName . Elm.valueOfLocated)))
    . over Elm.moduleUnions
        (filter (predicate . view (Elm.unionName . Elm.valueOfLocated)))
    . over (Elm.moduleExports . Elm.valueOfLocated . Elm._Explicit)
        (filter (shouldKeepExposing . view Elm.valueOfLocated))


subtypeReductions :: Src.Type -> Reductions Src.Type
subtypeReductions type_ =
    let
        subpartReduction subtype =
            [   ( subtype
                , Reduce.Description
                    { shortDescription = "Reduced type to one of its subparts"
                    , longDescription = unlines
                        [ "Before:"
                        , AST.prettyType type_
                        , ""
                        , "After:"
                        , AST.prettyType subtype
                        ]
                    }
                )
            ]

        typeF =
            view Elm.valueOfLocated (Type.deconstruct type_)
    in
    Reduce.Reductions type_ (foldMap subpartReduction typeF)


recordFieldReductions :: Src.Type -> Reductions Src.Type
recordFieldReductions =
    let
        description (fieldName, type_) =
            Reduce.Description
                { shortDescription = "Removed record type field " <> show fieldName
                , longDescription = unlines ["Its type was:", AST.prettyType type_]
                }
    in
    Elm.valueOfLocated (Elm._TRecord (Elm.recordTypeFields (Reduce.removeElements (Just . description))))


-- ensure, that we don't get any phantom type parameters for type aliases!
reduceTypeParams :: Elm.Alias -> Elm.Alias
reduceTypeParams alias =
    let
        usedTypeParams =
            Type.fold
                (Type.collectWith collectParam . view Elm.valueOfLocated)
                (view Elm.aliasType alias)

        collectParam = \case
            Type.Var name ->
                Set.singleton name

            _ ->
                Set.empty
    in
    over Elm.aliasTypeParams (filter ((`Set.member` usedTypeParams) . view Elm.valueOfLocated)) alias


reduceTypeAliases :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceTypeAliases =
    reduceElmModule (Elm.moduleAliases (traverse (fmap reduceTypeParams . Elm.aliasType (reduceRecursively recordFieldReductions))))


reduceToSubtypes :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceToSubtypes =
    let
        typeReductions =
            reduceRecursively (recordFieldReductions <> subtypeReductions)
    in
    reduceElmModule
        ( Elm.moduleAliases (traverse (fmap reduceTypeParams . Elm.aliasType typeReductions))
        <> Elm.moduleUnions (traverse (Elm.unionConstructors (traverse (_2 (traverse typeReductions)))))
        <> Elm.moduleDeclarations (traverse (Elm.valueOfLocated (Elm.declarationTypeAnnotation (traverse typeReductions))))
        )


reduceRecursively :: (Src.Type -> Reductions Src.Type) -> Src.Type -> Reductions Src.Type
reduceRecursively reduceFlat type_ =
    let
        onOriginal =
            Type.construct . (fmap . fmap) snd

        onRecursion =
            (fmap . fmap) fst
    in
    -- TODO: this is not explicitly recursive. But is it better?
    Type.foldWithOriginal
        (\typeF ->
            reduceFlat (onOriginal typeF)
            <> fmap Type.construct ((traverse . traverse) id (onRecursion typeF))
        )
        type_


unionReductions :: Set Name.Name -> Elm.Union -> Reductions Elm.Union
unionReductions referredConstructors union =
    let
        removeConstructor (constructorName, _) =
            if view Elm.valueOfLocated constructorName `Set.notMember` referredConstructors
                && length (view Elm.unionConstructors union) > 1
            then
                Just Reduce.Description
                    { shortDescription = "Removed constructor " <> show constructorName
                    , longDescription = ""
                    }
            else
                Nothing

        description fieldType =
            Reduce.Description
                { shortDescription = "Removed constructor field" -- TODO: Improve description. Indexed lenses?
                , longDescription = unlines ["Constructor field that was removed had type:", AST.prettyType fieldType]
                }

        constructorsReductions =
            Reduce.removeElements removeConstructor
            <> traverse (_2 (Reduce.removeElements (Just . description)))
    in
    Elm.unionConstructors constructorsReductions union

        
reduceUnions :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO Elm.Module
reduceUnions = reduceElmModule $ \module_ ->
    let
        referredConstructors =
            DefinitionDependencies.constructorReferences module_
                & Map.elems
                & map Set.fromList
                & Set.unions
    in
    Elm.moduleUnions (traverse (unionReductions referredConstructors))
        module_


-- TODO: Maybe get rid of this and just use Elm.Alias?
type AliasData = (Name.Name, [Ann.Located Name.Name], Src.Type)


reduceUnionsToAliases :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO ([AliasData], [AliasData])
reduceUnionsToAliases phaseName projectDir testCase reductionReport modulePath = do
    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        applyTypeAliasTransform alias@(nameToBeAliased, _, _) =
            applyFileTransform
                (addTypeAlias alias . removeTypeDeclaration nameToBeAliased)
                modulePath

        description (nameToBeAliased, _, _) =
            Reduce.Description
                { shortDescription = "Transformed type definition into type alias: " <> show nameToBeAliased
                , longDescription = ""
                }

        constructorReductions =
            concatMap viableReductions (view Elm.moduleUnions module_)

        viableReductions = \case
            Elm.Union _ name typeParams [(_, constructorParams)] ->
                case constructorParams of
                    [] ->
                        [(view Elm.valueOfLocated name, typeParams, Elm.dummyLocation Src.TUnit)]

                    [singleField] ->
                        [(view Elm.valueOfLocated name, typeParams, singleField)]

                    _ ->
                        []

            _ ->
                []

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = Transfer.oneByOneVariants
            , applyVariant = applyTypeAliasTransform . Reduce.delta
            , variantDescription = description . Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        (constructorReductions, [])


addTypeAlias :: AliasData -> Elm.Module -> Elm.Module
addTypeAlias (name, typeParams, type_) =
    over (Elm.moduleExports . Elm.valueOfLocated . Elm._Explicit)
        (++ [Elm.dummyLocation (Src.Upper name Src.Private)])
    . over (Elm.moduleAliases)
        (++ [Elm.Alias Elm.dummyRegion (Elm.dummyLocation name) typeParams type_])


reduceInlineAliases :: String -> FilePath -> IO Reduce.Interestingness -> IO String -> FilePath -> IO ([AliasData], [AliasData])
reduceInlineAliases phaseName projectDir testCase reductionReport modulePath = do
    module_ <- Elm.parseModuleFromFile Pkg.dummyName modulePath

    let
        applyTransform alias@(nameToBeInlined, _, _) =
            applyFileTransform
                (removeTypeDeclaration nameToBeInlined . inlineAlias alias)
                modulePath

        description (nameToBeInlined, typeParams, type_) =
            Reduce.Description
                { shortDescription = "Inlined type alias " <> show nameToBeInlined
                , longDescription =
                    unlines
                        [ "(type params: " <> unwords (map (Name.toString . view Elm.valueOfLocated) typeParams) <> ")"
                        , AST.prettyType type_
                        ]
                }

        aliasToData alias =
            ( view (Elm.aliasName . Elm.valueOfLocated) alias
            , view Elm.aliasTypeParams alias
            , view Elm.aliasType alias
            )

    Reduce.greedily
        Reduce.Reduction
            { phaseName = phaseName
            , variantsOf = Transfer.oneByOneVariants
            , applyVariant = applyTransform . Reduce.delta
            , variantDescription = description . Reduce.delta
            }
        testCase
        reductionReport
        projectDir
        (map aliasToData (view Elm.moduleAliases module_), [])


inlineAlias :: AliasData -> Elm.Module -> Elm.Module
inlineAlias (aliasName, typeParams, type_) =
    let
        applyTypeInline =
            Type.rebuild (over Elm.valueOfLocated inlineAlgebra)

        inlineAlgebra = \case
            Type.Type _ name applications
                | name == aliasName ->
                    type_
                        & substituteFree (Map.fromList (zip (map Ann.toValue typeParams) applications))
                        & Type.deconstruct
                        & view Elm.valueOfLocated

            other ->
                other
    in
    over (Elm.moduleAliases . traverse . Elm.aliasType) applyTypeInline
    . over (Elm.moduleUnions . traverse . Elm.unionConstructors . traverse . _2 . traverse) applyTypeInline
    . over (Elm.moduleDeclarations . traverse . Elm.valueOfLocated . Elm.declarationTypeAnnotation . _Just) applyTypeInline


substituteFree :: Map Name.Name Src.Type -> Src.Type -> Src.Type
substituteFree substitutions =
    let
        substituteAlg = \case
            Type.Var name ->
                case substitutions Map.!? name of
                    Just substitution ->
                        substitution
                            & Type.deconstruct
                            & view Elm.valueOfLocated

                    Nothing ->
                        Type.Var name

            other ->
                other
    in
    Type.rebuild (over Elm.valueOfLocated substituteAlg)
