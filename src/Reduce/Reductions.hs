{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}

module Reduce.Reductions where

-- misc
import Data.Bifunctor
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (maybeToList)
import Data.Function ((&))

import Language.Elm.OrphanInstances ()

import qualified Reduce


data Reductions a = Reductions
    { original :: a
    , reductions :: [(a, Reduce.Description)]
    } deriving (Show, Eq, Functor, Foldable)


instance Applicative Reductions where
    pure x = Reductions x []
    (Reductions f reductionsF) <*> (Reductions a reductionsA) =
        Reductions (f a)
            (map (first ($ a)) reductionsF <> map (first f) reductionsA)


instance Semigroup (Reductions a) where
    (Reductions a reductionsA) <> (Reductions _ reductionsB) =
        Reductions a (reductionsA <> reductionsB)


reduction :: a -> a -> String -> String -> Reductions a
reduction orig reduced short long =
    Reductions orig [(reduced, Reduce.Description short long)]


removeElements :: (a -> Maybe Reduce.Description) -> [a] -> Reductions [a]
removeElements removalDescription =
    let
        go = \case
            [] ->
                Reductions [] []

            (x:xs) ->
                (case removalDescription x of
                    Just description ->
                        Reductions (x:xs) [(xs, description)]

                    Nothing ->
                        Reductions (x:xs) []
                )
                <> fmap ((:) x) (go xs)
    in
    go


removeMapElements :: (k -> v -> Maybe Reduce.Description) -> Map k v -> Reductions (Map k v)
removeMapElements removalDescription =
    Map.traverseMaybeWithKey
        (\k v ->
            removalDescription k v
                & fmap ((,) Nothing)
                & maybeToList
                & Reductions (Just v)
        )
