{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}

module CommandLineInterface where

import qualified Data.List as List
import Data.Function ((&))
import qualified Data.Maybe as Maybe
import qualified Data.List.NonEmpty as NonEmpty
import Data.List.NonEmpty (NonEmpty(..))
import qualified System.IO as System
import qualified System.Exit as System
import Options.Applicative
import Options.Applicative.Help.Pretty


data Options = Options
    { testscript :: String
    , testDir :: String
    , mainModule :: String
    , customTimeout :: Int
    , noVendoring :: Bool
    , passOrder :: NonEmpty String
    } deriving Show


handleArgs :: NonEmpty String -> IO Options
handleArgs defaultPasses = do
    execParser (info (optionsAndTutorialParser defaultPasses <**> helper) elmReduceDesc)
        >>= Maybe.maybe (printTutorial >> System.exitSuccess) return


elmReduceDesc :: InfoMod a
elmReduceDesc =
    fullDesc
    <> header "elm-reduce - Cut down elm codebases to interesting minimal sources"
    <> progDesc
        ( "Reduce the codebase in given working directory, while ensuring "
        <> "given testscript still returns exit code 0.\n"
        <> "Example: elm-reduce ./testscript --main-module Main\n"
        <> "For a tutorial, run with --tutorial."
        )


optionsParser :: NonEmpty String -> Parser Options
optionsParser defaultPasses = Options
    <$> argument str
        ( metavar "TESTSCRIPT"
        <> help
            ( "The script to test the current codebase state for interestingness. "
            <> "This script should return exit code 0 for interesting codebases. "
            <> "For more info read the --help text."
            )
        <> action "command"
        )
    <*> strOption
        ( long "working-dir"
        <> metavar "DIRECTORY"
        <> help "Target for the greeting"
        <> showDefault
        <> value "."
        )
    <*> strOption
        ( long "main-module"
        <> short 'm'
        <> metavar "MODULENAME"
        <> help "The main module name, which will ultimately contain the minimal, reduced testcase. Usually the module with the main function (entry point)."
        )
    <*> option auto
        ( long "timeout"
        <> metavar "MILLISECONDS"
        <> help "Time in milliseconds to allow the testscript to execute before killing it."
        <> showDefault
        <> value 20000
        )
    <*> switch
        ( long "no-vendoring"
        <> help "When set, elm-reduce will skip the initial step of copying dependencies inline."
        )
    <*>
        (many
            (strOption
                ( long "add-pass"
                <> short 'p'
                <> metavar "PASSNAME"
                <> completeWith (List.nub (NonEmpty.toList defaultPasses))
                <> (helpDoc $ Just $
                    vcat
                        [ t "For customizing the passes and pass order."
                        , t "The default pass order contains all available passes and is:"
                        , indent 1 $ vcat (map (\name -> "-" <+> text name) (NonEmpty.toList defaultPasses))
                        , t "This flag has to be repeated for every pass you want to add."
                        , t "The order of them appearing in the command line arguments will determine the order of their execution."
                        ]
                    )
                )
            )
            & fmap (Maybe.fromMaybe defaultPasses . NonEmpty.nonEmpty)
        )


optionsAndTutorialParser :: NonEmpty String -> Parser (Maybe Options)
optionsAndTutorialParser defaultPasses =
    flag' Nothing
        ( long "tutorial"
        <> help "Print a tutorial for how to use elm-reduce."
        )
    <|> fmap Just (optionsParser defaultPasses)


printTutorial :: IO ()
printTutorial =
    tutorial
        & renderSmart 1 80
        & displayIO System.stdout


tutorial :: Doc
tutorial =
    vcat
        [ bold $ t "elm-reduce: Cut down elm codebases to interesting minimal sources."
        , ""
        , t "This program was mainly developed for improving compiler bug reports automatically."
        <+> t "It can be used for different purposes as well, for example"
        <+> t "reducing bug reports for elm tooling, but it might not work as well."
        , t "Note that you need to have 'git' installed and in your PATH for elm-reduce to work."
        , ""
        , t "Say, you have an elm project and you run `elm make src/Main.elm --debug`,"
        <+> t "but the compiler crashes during compilation with the error"
        <+> t "`elm: Map.!: given key is not an element in the map`."
        <+> t "It is likely, that this error can be reproduced with a program of only"
        <+> t "5-15 lines of code, but reducing your current project manually"
        <+> t "might be really difficult or tiresome."
        , t "With elm-reduce, you need to create an executable that checks whether the codebase currently"
        <+> t "reproduces the bug. For our example, you would write a bash script looking like this:"
        , ""
        , indent 4 $ vcat
            [ "#!/bin/bash"
            , "EXPECTED_ERR=\"elm: Map.!: given key is not an element in the map\""
            , "elm make --output=elm.js --debug src/Main.elm 2>&1 |"
            , "  grep -qF \"$EXPECTED_ERR\""
            ]
        , ""
        , t "Note, that `grep -q` exits with exit code 0 on the first occurance of given string,"
        <+> t "and exits with 1, when the given string cannot be found."
        , t "Now we save such a file in our project's working directory as `testscript.sh` and mark it as executable using"
        <+> t "`chmod +x testscript.sh`."
        , bold (t "BEFORE WE RUN ELM-REDUCE") <+> t "we have to ensure, that the current working directory is backed up by"
        <+> t "a git repository. We check this via `git status`."
        , t "If we're not in a git repository we initialize one using `git init`."
        , t "It might be preferable to switch to a new branch, since elm-reduce creates thousands of commits:"
        <+> t "`git checkout -b bug-report-reduced`"
        , ""
        , t "Now, we're ready to execute elm-reduce:"
        , ""
        , indent 4 "elm-reduce ./testscript.sh --main-module Main"
        , ""
        , t "Elm-reduce will now proceed with vendoring all dependencies and reducing the codebase by trying to"
        <+> t "remove parts of the code smartly and ensuring the testscript still returns with exit code 0."
        , t "This might take a while (between 10 minutes and several hours, depending mostly on project size and"
        <+> t "testscript execution time, see the help text about timeouts). There is no need to manually"
        <+> t "kill elm-reduce, but also no harm in doing so, since all changes are saved in the git repository."
        , t "If you killed elm-reduce during execution, and want to resume it, simply `git reset --hard HEAD` in"
        <+> t "elm-reduce's branch and re-run the elm-reduce command."
        , ""
        , t "For more information and configurability, look at elm-reduce's help text via --help."
        ]


t :: String -> Doc
t = fillSep . map text . words
